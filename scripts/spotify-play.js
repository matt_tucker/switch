



var config = {
    host: 'REDACTED',
    port: 22,
    username: 'REDACTED',
    password: 'REDACTED',
    tryKeyboard: true,
    readyTimeout: 60000,
}

var Client = require('ssh2').Client;

var conn = new Client();


spotifyAction = function(callback) {
    console.log('Starting spotify...')

    conn.on('ready', function() {
        // console.log('Client :: ready');
        conn.exec('osascript ~/SpotifyControl/SpotifyControl.scpt play', function(err, stream) {
            if (err) {
                callback(err)
            }
            stream.on('close', function(code, signal) {
                // console.log('Stream :: close :: code: ' + code + ', signal: ' + signal);
                conn.end();
            }).on('data', function(data) {
                console.log('STDOUT: ' + data);
            }).stderr.on('data', function(data) {
                console.log('STDERR: ' + data);
            });
            callback(null)
        });
    })

    .on('keyboard-interactive',
            function(name, instructions, instructionsLang, prompts, finish) {
                finish([config.password]);
            })
        .connect(config);
}


var async = require("async");
async.series([
    spotifyAction
], function(error, results) {
    if (error) {
        console.log('Errors:', error)
    }
    console.log(results);
});


// function(callback) {
//     setTimeout(function() {
//         console.log(“Task 3“);
//         callback(null, 3);
//     }, 100);
// }
