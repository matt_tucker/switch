var TV = {
    power: "sendir,1:2,1,37878,1,1,171,171,21,64,21,64,21,64,21,22,21,22,21,22,21,22,21,22,21,64,21,64,21,64,21,22,21,22,21,22,21,22,21,22,21,22,21,64,21,22,21,22,21,22,21,22,21,22,21,22,21,64,21,22,21,64,21,64,21,64,21,64,21,64,21,64,21,3787\r",
    netflix: "sendir,1:2,1,37878,1,1,171,171,21,64,21,64,21,64,21,22,21,22,21,22,21,22,21,22,21,64,21,64,21,64,21,22,21,22,21,22,21,22,21,22,21,64,21,64,21,22,21,22,21,64,21,64,21,64,21,64,21,22,21,22,21,64,21,64,21,22,21,22,21,22,21,22,21,1779,171,171,21,64,21,64,21,64,21,22,21,22,21,22,21,22,21,22,21,64,21,64,21,64,21,22,21,22,21,22,21,22,21,22,21,64,21,64,21,22,21,22,21,64,21,64,21,64,21,64,21,22,21,22,21,64,21,64,21,22,21,22,21,22,21,22,21,1779,171,171,21,64,21,64,21,64,21,22,21,22,21,22,21,22,21,22,21,64,21,64,21,64,21,22,21,22,21,22,21,22,21,22,21,64,21,64,21,22,21,22,21,64,21,64,21,64,21,64,21,22,21,22,21,64,21,64,21,22,21,22,21,22,21,22,21,3787\r"
};
var STEREO = {
    power: "sendir,1:1,1,40192,1,1,96,24,48,24,24,24,48,24,24,24,48,24,24,24,24,24,24,24,24,24,24,24,24,24,48,1035,96,24,48,24,24,24,48,24,24,24,48,24,24,24,24,24,24,24,24,24,24,24,24,24,48,1034,96,24,48,24,24,24,48,24,24,24,48,24,24,24,24,24,24,24,24,24,24,24,24,24,48,4019\r",
    dvd: "sendir,1:1,1,40192,1,1,96,24,48,24,24,24,48,24,48,24,48,24,48,24,48,24,24,24,24,24,24,24,24,24,48,965,96,24,48,24,24,24,48,24,48,24,48,24,48,24,48,24,24,24,24,24,24,24,24,24,48,965,96,24,48,24,24,24,48,24,48,24,48,24,48,24,48,24,24,24,24,24,24,24,24,24,48,4019\r",
    video1: "sendir,1:1,1,40192,1,1,96,24,24,24,48,24,24,24,24,24,24,24,48,24,24,24,24,24,24,24,24,24,24,24,48,1059,96,24,24,24,48,24,24,24,24,24,24,24,48,24,24,24,24,24,24,24,24,24,24,24,48,1059,96,24,24,24,48,24,24,24,24,24,24,24,48,24,24,24,24,24,24,24,24,24,24,24,48,4019\r"
};




var Itach = require('simple-itach');
var itach = new Itach('REDACTED');


var cmd = [TV.power, STEREO.power, 5000, STEREO.video1]



itachAction = function(callback) {
    console.log('Setting up tv and stereo...')
    itach.send(cmd, function(err, res) {
        if (err) {
            console.log(err);
        } else {
            // command has been successfully transmitted to your iTach
            console.log('itach command successfully sent.', res)
        }
    });
}



var config = {
    host: 'REDACTED',
    port: 22,
    username: 'REDACTED',
    password: 'REDACTED',
    tryKeyboard: true,
    readyTimeout: 60000,
}

var Client = require('ssh2').Client;

var conn = new Client();


spotifyAction = function(callback) {
    console.log('Starting spotify...')

    conn.on('ready', function() {
        // console.log('Client :: ready');
        conn.exec('osascript ~/SpotifyControl/SpotifyControl.scpt play', function(err, stream) {
            if (err) {
                callback(err)
            }
            stream.on('close', function(code, signal) {
                // console.log('Stream :: close :: code: ' + code + ', signal: ' + signal);
                conn.end();
            }).on('data', function(data) {
                console.log('STDOUT: ' + data);
            }).stderr.on('data', function(data) {
                console.log('STDERR: ' + data);
            });
            callback(null)
        });
    })

    .on('keyboard-interactive',
            function(name, instructions, instructionsLang, prompts, finish) {
                finish([config.password]);
            })
        .connect(config);
}


var async = require("async");
async.series([
    itachAction,
    spotifyAction
], function(error, results) {
    if (error) {
        console.log('Errors:', error)
    }
    console.log(results);
});


// function(callback) {
//     setTimeout(function() {
//         console.log(“Task 3“);
//         callback(null, 3);
//     }, 100);
// }
