# SWITCH #

Switch is an open-source home automation system specifically designed for the BeagleBone Black.  This repository contains the server software to control Z-Wave devices from a usb Z-Wave stick running on a BeagleBone Black Rev. C development platform.  The server software provides user management, device discovery, control, and scheduling.

The server is written in javascript for the Node.js runtime environment. The front end is written using the Angular.js framework.


## Hardware Required ##
The following is the hardware I used to create the system, but the code could easily
be ported to any linux based system. For reference the cost of the system is
approximately $100 (excluding Z-Wave devices), however setting this up requires 
expert linux skills.

1. BeagleBone Black Rev. C
2. Aeon Labs DSA02203-ZWUS Z-Wave Z-Stick Series 2 USB Dongle



## Installation ##

Setup the BeagleBone Black and attach it to your network via an ethernet cable.

### Optional: Change the hostname of the BBB to switch
    ssh root@beaglebone.local
    change hostname in /etc/hostname to switch


### Create the deploy user
First create a dedicated user to run the server. Also give the deploy user sudo privileges.

    useradd -d /srv/deploy -m -s /bin/bash deploy
    passwd deploy
    adduser deploy sudo


### Install Software Dependencies
Next update the system, and then install the required supporting software packages.

    sudo apt-get update
    sudo apt-get ntp
    sudo apt-get install libudev-dev
    sudo apt-get install ruby-full
    sudo gem install sass
    sudo npm install imagemin -g
    sudo npm install grunt-cli -g
    sudo npm install bower -g

### Set up your time zone.
    sudo dpkg-reconfigure tzdata

### Set up the Z-Wave Stick
1. Run sudoedit /etc/udev/rules.d/50-ttyusb.rules and add the following line:
    KERNEL=="ttyUSB[0-9]*",NAME="tts/USB%n",SYMLINK+="%k",GROUP="uucp",MODE="0666"

2. Plugin Z-Wave stick
3. Reboot



### Openzwave libraries
Download, make and install the openzwave libraries.

    cd /srv/deploy
    git clone https://github.com/OpenZWave/open-zwave.git
    cd open-zwave
    make                    # takes awhile
    sudo make install

### Clone the switch repository from git
    git clone https://matt_tucker@bitbucket.org/matt_tucker/switch.git
    cd switch
    npm install
    bower install
    grunt build
    grunt serve  # test


### Create the Switch service
    cd /etc/init.d
    sudo cp /srv/deploy/switch.sh /etc/init.d
    sudo mv /etc/init.d/switch.sh /etc/init.d/switch
    sudo chmod +x /etc/init.d/switch
    sudo update-rc.d switch defaults

    sudo touch /var/log/switch.log
    sudo chown deploy switch.log


### Start the service
    sudo service switch start

### Adding a Z-Wave device ###
1. Position the Z-Stick within 1 foot of the device you wish to add, and press the Z-Stick button once. A circular blue light around the button will begin to blink slowly, indicating the Z-Stick is now in “add mode”.

2. With the Z-Stick in add mode, press the control button on the Z-Wave device you wish to add. The blinking blue light will turn solid blue when the device is successfully added.

3. After 2-3 seconds, the Z-Stick will go back into add mode again to allow the inclusion of more Z-Wave devices. Press the Z-Stick button once to exit add mode.

The Switch software will automatically discover new devices and display them in the dashboard.




### Technologies ###
Node.js, Angular.js, TingoDB, Grunt, BeagleBone Black,

[Angular Materiel] https://material.angularjs.org/latest/