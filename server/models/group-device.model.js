
var zwave = require('../zwave/zwave')
var Device = require('./device.model.js')

function GroupDevice(obj) {
    Device.call(this, obj);


    this.node_id = obj.node_id;
    this.type = 'device.zwave.switch'
};

GroupDevice.prototype = Object.create(Device.prototype);

GroupDevice.prototype.constructor = GroupDevice;

GroupDevice.prototype.setSwitch = function(value) {

    if (value) {
        // zwave.switchOn(this.node_id)
        zwave.setValue(this.node_id, 37, 1, 0, true); // turn on 1st relay

    } else {
        // zwave.switchOff(this.node_id)
        zwave.setValue(this.node_id, 37, 1, 0, false); // turn on 1st relay
    }

}

module.exports = GroupDevice;
