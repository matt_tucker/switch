'use strict'
var util = require("util");
var _ = require('lodash')
var later = require('later')
later.date.localTime();

function BaseScheduleManager() {

    //Dictionary of scheduled jobs. Key is the owner of the jobs
    this.jobs = {};
}

/**
 * Clear and remove jobs owned by id.
 */
BaseScheduleManager.prototype.removeSchedules = function(id) {
    console.log('removeSchedules for device ' + id)
    if (!this.jobs[id]) {
        return;
    }

    _.each(this.jobs[id], function(job) {
        job.clear()
    })

    this.jobs[id] = [];
}

/**
 * Schedule time events.
 */
BaseScheduleManager.prototype.schedule = function(id, eventListeners) {
    console.log('scheduling events for device ' + id, eventListeners)
    var manager = this;

    _.each(eventListeners, function(eventListener) {
        if (!eventListener.schedule) {
            return;
        }
        var job = function(event) {
            return function() {
                console.log('Running event listener for event:', event)
                return eventListener.listener(event);
            }
        }(eventListener.event);

        manager.addSchedule(id, eventListener.schedule, job)
    })

}

BaseScheduleManager.prototype.addSchedule = function(id, schedule, listener) {
    try {
        if (!this.jobs[id]) {
            this.jobs[id] = []
        }

        // console.log('Scheduling job at ', schedule.schedules)
        console.log('Next schedule event:', later.schedule(schedule).next(1))

        var job = later.setInterval(listener, schedule)

        //add job, so we can potentially cancel it at a later time
        this.jobs[id].push.apply(this.jobs[id], job)
    } catch (err) {
        console.log('Failed to add schedule.', schedule)
    }

}


module.exports = BaseScheduleManager
