var Action = require('./action.model')


function JavascriptAction(obj) {
    Action.call(this, obj);
    this.script = obj.script;
};


JavascriptAction.prototype = Object.create(Action.prototype);

JavascriptAction.prototype.constructor = JavascriptAction;

JavascriptAction.prototype.type = 'action.javascript'

//Implement Abstract Methods
JavascriptAction.prototype.execute = function(devicesDict) {
    eval(this.script)
}


Action.register(JavascriptAction.prototype.type, JavascriptAction)

module.exports = JavascriptAction;
