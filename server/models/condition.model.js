'use strict'
var _ = require('lodash')

/**
 * Condition class
 */
function Condition(obj) {
    // console.log('inflating condition', obj)
}


// Abstract
Condition.prototype.isValid = function(devicesDict, event) {
    alert('isValid in Condition not implemented');
};

Condition.prototype.isTime = function() {
    return false;
}

Condition.prototype.getDeviceId = function() {
    return null;
}

Condition.prototype.getEvent = function() {
    return null;
}


Condition.isAllValid = function(conditions, devicesDict, event) {

    //test the conditions.
    _.each(conditions, function(condition) {
        if (!condition.isValid(devicesDict, event)) {
            return false;
        }
    })

    return true
};


//Polymorphism
var models = {}
Condition.create = function(obj) {
    if (!_.has(models, obj.type)) {
        console.log('models:', models)
        throw "Condition has an unregistered type: " + obj.type
    }

    var condition = new models[obj.type](obj);

    return condition;
}

Condition.register = function(type, model) {
    models[type] = model;
}

module.exports = Condition
