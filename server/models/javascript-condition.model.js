var Condition = require('./condition.model')
var _eval = require('eval')

function JavascriptCondition(obj) {
    Condition.call(this, obj);

    this.script = obj.script;

    try {
        this.data = _eval(this.script);
    } catch (err) {
        console.error('Unable to evaluate condition. ' + err)
        this.data = {}
    }
    console.log('Javascript condition Data', this.data)
};


JavascriptCondition.prototype = Object.create(Condition.prototype);

JavascriptCondition.prototype.constructor = JavascriptCondition;

JavascriptCondition.prototype.type = 'condition.javascript'


JavascriptCondition.prototype.getDeviceId = function() {
    return this.data.deviceId;
}

JavascriptCondition.prototype.getEvent = function() {
    return this.data.event;
}
JavascriptCondition.prototype.isTime = function() {
    return this.data.isTime;
}

JavascriptCondition.prototype.isValid = function(devicesDict, event) {
    if (this.data.isValid) {
        return this.data.isValid(devicesDict, event)
    } else {
        return false;
    }
};

Condition.register(JavascriptCondition.prototype.type, JavascriptCondition)

module.exports = JavascriptCondition;
