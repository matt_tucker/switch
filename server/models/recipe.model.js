'use strict'
var _ = require('lodash')

var DbDevice = require('./db-device.model');
var Device = require('./device.model');
var Condition = require('./condition.model')
var Action = require('./action.model')

/**
 * Recipe class
 */
function Recipe(obj) {
    this.conditions = _.map(obj.conditions, function(condition) {
        return Condition.create(condition);
    });

    this.actions = _.map(obj.actions, function(action) {
        return Action.create(action);
    });
}

Recipe.prototype.getDeviceIds = function() {
    var deviceIds = [];
    _.each(this.conditions, function(condition) {
        var deviceId = condition.getDeviceId();
        if (deviceId) {
            deviceIds.push(deviceId);
        }
    });

    _.each(this.actions, function(action) {
        var deviceId = action.getDeviceId();

        if (deviceId) {
            deviceIds.push(deviceId);
        }
    });

    return deviceIds;
}

Recipe.prototype.run = function() {
    var deviceIds = this.getDeviceIds()
    var self = this;
    DbDevice.find()
        .where('_id')
        .in(deviceIds)
        .exec(function(err, docs) {

            var devices = _.map(docs, function(device) {
                return Device.create(device);
            })

            var devicesDict = _.indexBy(devices, '_id');

            Action.executeAll(self.actions, devicesDict);

        });

}

Recipe.prototype.getEventListeners = function() {
    var self = this;
    var eventlisteners= _.map(this.conditions, function(condition) {

        var eventListener = {}
        eventListener.event = condition.getEvent()

        if (!eventListener.event) {
            // console.warn('Condition does not have an event listener')
            return null;
        }

        eventListener.listener = function(recipe) {

            return function(event) {
                console.log('Processing recipe...')
                //get device dependencies
                var deviceIds = recipe.getDeviceIds()

                DbDevice.find()
                    .where('_id')
                    .in(deviceIds)
                    .exec(function(err, docs) {

                        var devices = _.map(docs, function(device) {
                            return Device.create(device);
                        })

                        var devicesDict = _.indexBy(devices, '_id');

                        if (Condition.isAllValid(recipe.conditions, devicesDict, event)) {
                            Action.executeAll(recipe.actions, devicesDict);
                        }

                    });

            };

        }(self);

        // if (condition instanceof TimeCondition) {
        if (condition.isTime()) {
            var schedule = condition.getSchedule()
            eventListener.schedule = schedule
        }

        return eventListener;

    })

    eventlisteners = _.pick(eventlisteners, function(eventlistener) {
        return eventlistener != null
    })

    return eventlisteners


}


module.exports = Recipe
