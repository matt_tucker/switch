/**
 * ScheduleCondition class
 */
 'use strict';
 var Condition = require('./condition.model.js')

function ScheduleCondition(obj) {
    Condition.call(this, obj);
    this.type = 'condition.schedule'


    this.schedule = obj.schedule;
};

ScheduleCondition.prototype = Object.create(Condition.prototype);

ScheduleCondition.prototype.constructor = ScheduleCondition;

//Implement Abstract Methods
ScheduleCondition.prototype.isValid = function(devices, options) {
    return true;
}

ScheduleCondition.prototype.isTime = function() {
    return true;
}

ScheduleCondition.prototype.getEvent = function() {
    return 'schedule';
}

ScheduleCondition.prototype.getSchedule = function() {
    return this.schedule;
}

module.exports = ScheduleCondition;
