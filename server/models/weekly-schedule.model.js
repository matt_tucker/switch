var Recipe = require('./recipe.model')
var Condition = require('./condition.model')
var later = require('later')
var _ = require('lodash')

/**
 * WeeklySchedule class
 */
function WeeklySchedule(obj) {

    // this.dows = obj.dows
    this.recipes = _.map(obj.recipes, function(obj) {
        var recipe = new Recipe(obj);

        // console.log('ws recipe', recipe)
        return recipe
    });
}

// function isoDowsToLaterDows(isoDows) {

//     // ISO
//     // dow 1-7, 1=Monday

//     // LATER
//     // dow of week 1-7, 1 = sunday,

//     laterDows=_.map(isoDows, function(isoDow) {
//         if (isoDow == 7) {
//             return 1;
//         } else {
//             return isoDow + 1;
//         }
//     })
//     laterDows.sort()
//     return laterDows;
// }

WeeklySchedule.prototype.getEventListeners = function() {

    var eventListeners = []

    // var dows = this.dows;

    _.each(this.recipes, function(recipe) {
        // console.log('conditions', recipe.conditions, recipe.actions)

        // //Get the first time condition. (There should be only one.)
        // var index = _.findIndex(recipe.conditions, function(condition) {
        //     return condition.isTime();
        // });

        // if (index == -1) {
        //     console.warn("Failed to find time condition to schedule on in WeeklySchedule.")
        //     return;
        // }

        // var condition = recipe.conditions[index]

        // var schedule = condition.getSchedule();

        // //Inject the weekly schedule dows into the schedule
        // schedule.schedules.dw = isoDowsToLaterDows(dows);

        // // console.log('sched', schedule, dows)


        // //Create the task to execute at the scheduled time.
        // var listener = function(recipe, event) {

        //     return function() {
        //         console.log('Executing Recipe')
        //         if (Condition.isAllValid(recipe.conditions, event) {
        //                 Action.execute(recipe.actions)
        //             // console.log('Executing actions..')
        //             // _.each(recipe.actions, function(action) {
        //             //     action.execute(devices)
        //             // })
        //         }

        //     }
        // }(recipe, event);

        // // console.log('Creating task to go at', schedule)
        // // var job = later.setInterval(task, schedule)
        // eventListeners.push({event:event, listener:listener, schedule:schedule});

        var listenersDict = recipe.getEventListeners();
        eventListeners.push.apply(eventListeners, _.values(listenersDict) )
    })


    return eventListeners;
}

module.exports = WeeklySchedule
