
var Condition = require('./condition.model')
var ScheduleCondition = require('./schedule-condition.model')
var SolarCondition = require('./solar-condition.model')
var JavascriptCondition = require('./javascript-condition.model')
var EventCondition = require('./event-condition.model')


Condition.register('condition.schedule', ScheduleCondition)
Condition.register('condition.solar', SolarCondition)



