
var zwave = require('../zwave/zwave')
var Device = require('./device.model.js')

function SwitchZwaveDevice(obj) {
    Device.call(this, obj);


    this.node_id = obj.node_id;
    this.type = 'device.zwave.switch'
};

SwitchZwaveDevice.prototype = Object.create(Device.prototype);

SwitchZwaveDevice.prototype.constructor = SwitchZwaveDevice;

SwitchZwaveDevice.prototype.setSwitch = function(value) {

    zwave.setValue(this.node_id, 37, 1, 0, value); // turn on 1st relay

}

module.exports = SwitchZwaveDevice;
