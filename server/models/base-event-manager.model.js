    'use strict'
var util = require('util');
var EventEmitter = require('events').EventEmitter;
var _ = require('lodash')



function BaseEventManager() {
    this.emitter = new EventEmitter();

    this.listeners = {}
}

BaseEventManager.prototype.emit = function(event) {
    this.emitter.emit(event)
}

BaseEventManager.prototype.removeListener = function(id, event) {
    if (!this.listeners[id] || !this.listeners[id][event]) {
        return;
    }

    this.emitter.removeListener(event, this.listeners[id][event])
    delete this.listeners[id][event]
}

BaseEventManager.prototype.addListener = function(id, event, listener) {
    this.listeners[id] = this.listeners[id] || {}

    this.listeners[id][event] = listener;
    this.emitter.on(event, listener)
}

BaseEventManager.prototype.removeListeners = function(id) {
    if (!this.listeners[id]) {
        return;
    }
    console.log('remove listeners: ', id, this.listeners[id])
    for (var event in this.listeners[id]) {
        this.removeListener(event, this.listeners[id][event])
    }

}

BaseEventManager.prototype.setListeners = function(id, eventListeners) {
    var self = this;
    this.removeListeners(id)

    _.map(eventListeners, function(eventListener) {
        self.addListener(id, eventListener.event, eventListener.listener)
    })
}



module.exports = BaseEventManager
