'use strict'
var _ = require('lodash')

var DbDevice = require('./db-device.model');

// DON"T INCLUDE, CIRCULAR!
// var WeeklySchedule = require('./weekly-schedule.model')

/**
 * Device class
 */
function Device(obj) {
    this._id = obj._id;
    this.name = obj.name;
    this.location_id = obj.location_id;
    this.weekly_schedules = obj.weekly_schedules; //don't inflate schedules due to circular dependency
    this.image_url = obj.image_url;
}

// Db Wrapper
Device.save = function(cb) {
    var dbDevice = new DbDevice(this);
    dbDevice.save(cb)
}

// Abstract


//Polymorphism
var models = {}
Device.create = function(object) {
    try {
        var device = new models[object.type](object);
    } catch (err) {

        console.error("Unable to create device model for "+ object.type ,err)
    }
    return device;
}


Device.register = function(type, model) {
    models[type] = model;
}

module.exports = Device;
