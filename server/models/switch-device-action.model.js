var Action = require('./action.model')

function SwitchDeviceAction(obj) {
    Action.call(this, obj);
    this.deviceId = obj.deviceId;

    this.value = obj.value;
    this.type = 'action.device.zwave.switch'
};



SwitchDeviceAction.prototype = Object.create(Action.prototype);

SwitchDeviceAction.prototype.constructor = SwitchDeviceAction;

SwitchDeviceAction.prototype.getDeviceId = function() {
    return this.deviceId;
}

//Implement Abstract Methods
SwitchDeviceAction.prototype.execute = function(devicesDict) {
    var device = devicesDict[this.deviceId]
    if( device) {
        device.setSwitch(this.value);
    } else {
        console.warn('Device not found:', this.deviceId)
        console.log(devicesDict)
    }


}

module.exports =SwitchDeviceAction;
