/**
 * SolarCondition class
 */
'use strict';
var moment = require('moment')
var SunCalc = require('suncalc')

var Condition = require('./condition.model.js')


function SolarCondition(obj) {
    console.log('Creating SolarCondition: ', obj)
    Condition.call(this, obj);

    this.type = 'condition.solar'
    this.event = obj.event;
    this.dows = obj.dows;
};

SolarCondition.prototype = Object.create(Condition.prototype);

SolarCondition.prototype.constructor = SolarCondition;

//Implement Abstract Methods
SolarCondition.prototype.isValid = function(devices, options) {
    return true;
}

SolarCondition.prototype.isTime = function() {
    return true;
}

SolarCondition.prototype.getEvent = function() {
    return this.event;
}


SolarCondition.prototype.getSchedule = function() {
    //todo: put in settings!!!
    var times = SunCalc.getTimes(new Date(), 40.0274, -105.2519);
    var time = times[this.event]

    var mtime = moment(time)
    var hour = mtime.hours()
    var minute = mtime.minutes()
    var schedule = {
        schedules: [{
            h: [hour],
            m: [minute],
            dw: this.dows
        }]
    }

    console.log('solar schedule:', schedule)
    return schedule;
}

module.exports = SolarCondition;
