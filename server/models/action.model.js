var _ = require('lodash')

/**
 * Action class
 */



function Action(obj) {

}


// Abstract
Action.prototype.execute = function(devicesDict) {
    throw 'execute in Action not implemented';
};

Action.prototype.getDeviceId = function() {
    return null;
}

//Polymorphism
var models ={}
Action.create = function(object) {
    if (!_.has(models, object.type)) {
        console.log('models:', models)
        throw "Action has an unregistered type: " + object.type
    }

    return new models[object.type](object);
}

Action.register = function(type, model) {
    //mkt todo: for security should we allow over writing?
    models[type] = model;
}


Action.executeAll = function(actions, devicesDict) {
    _.map(actions, function(action){
        console.log('Executing action', action, action.deviceId)
        action.execute(devicesDict)
    })
};

module.exports =Action
