'use strict';

var mongoose = require('mongoose'),
    Schema = mongoose.Schema,
    ObjectId = Schema.ObjectId;

var RecipeSchema = new Schema({
    type: String,
    name: String,
    conditions: Array,
    actions: Array,
    image_url: String
});


module.exports = mongoose.model('Recipe', RecipeSchema);
