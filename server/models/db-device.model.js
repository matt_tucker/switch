'use strict';

var mongoose = require('mongoose'),
    Schema = mongoose.Schema,
    ObjectId = Schema.ObjectId;

var DeviceSchema = new Schema({
    type: String,
    name: String,
    node_id: Number,
    location_id: ObjectId,
    weekly_schedules: Array,
    image_url: String
});


module.exports = mongoose.model('Device', DeviceSchema);
