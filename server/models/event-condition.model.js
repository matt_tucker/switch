var Condition = require('./condition.model')

function EventCondition(obj) {
    Condition.call(this, obj);
    this.sourceId = obj.sourceId;
    this.sourceType = obj.sourceType;
    this.event = obj.event;
};



EventCondition.prototype = Object.create(Condition.prototype);

EventCondition.prototype.constructor = EventCondition;

EventCondition.prototype.getDeviceId = function() {
    //todo: change to getSrcId?
    return this.sourceId;
}

EventCondition.prototype.type = 'condition.event'

//Implement Abstract Methods
EventCondition.prototype.getEvent = function() {
    return this.sourceId + '_' + this.event;
}
EventCondition.prototype.isTime = function() {
    return false;
}

EventCondition.prototype.isValid = function(devicesDict, event) {
    return event === this.getEvent();
};

Condition.register(EventCondition.prototype.type, EventCondition)

module.exports =EventCondition;
