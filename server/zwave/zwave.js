/**
 * Zwave configuration
 */

'use strict';

var _ = require('lodash');
var OpenZWave = require('openzwave-shared');
// var OpenZWave = require('openzwave');

var Node = function() {
    this.manufacturer ='';
    this.manufacturerid ='';
    this.product= '';
    this.producttype= '';
    this.productid= '';
    this.type= '';
    this.name= '';
    this.loc= '';
    this.classes= {};
    this.ready= false;
}


var zwave = new OpenZWave({
    Logging: false,     // disable file logging (OZWLog.txt)
    ConsoleOutput: true // enable console logging
});



zwave.isReady = false;

zwave.nodes = [];

zwave.on('driver ready', function(homeid) {
    console.log('Driver ready for homeid=0x%s...', homeid.toString(16));
    zwave.isReady = true;
});

zwave.on('driver failed', function() {
    console.warn('Failed to start zwave driver.');
    zwave.disconnect();
});

zwave.on('node added', function(nodeid) {
    console.log('zwave:node added: %d', nodeid)
    zwave.nodes[nodeid] = new Node();
});

zwave.on('value added', function(nodeid, comclass, value) {
    console.log('zwave:value added: node%d, %s = %s', nodeid, comclass, value['value'])
    if (!zwave.nodes[nodeid]['classes'][comclass])
        zwave.nodes[nodeid]['classes'][comclass] = {};
    zwave.nodes[nodeid]['classes'][comclass][value.index] = value;
});

zwave.on('value changed', function(nodeid, comclass, value) {
    if (zwave.nodes[nodeid]['ready']) {
        console.log('zwave:value changed: node%d: changed: %s:%s:%s->%s', nodeid, comclass,
            value['label'],
            zwave.nodes[nodeid]['classes'][comclass][value.index]['value'],
            value['value']);
    }
    zwave.nodes[nodeid]['classes'][comclass][value.index] = value;
});

zwave.on('value removed', function(nodeid, comclass, index) {
    if (zwave.nodes[nodeid]['classes'][comclass] &&
        zwave.nodes[nodeid]['classes'][comclass][index])
        delete zwave.nodes[nodeid]['classes'][comclass][index];
});

zwave.on('node ready', function(nodeid, nodeinfo) {
    console.log('zwave:node ready: node%d', nodeid)
    var nodeinfo = _.cloneDeep(nodeinfo);
    zwave.nodes[nodeid]['manufacturer'] = nodeinfo.manufacturer;
    zwave.nodes[nodeid]['manufacturerid'] = nodeinfo.manufacturerid;
    zwave.nodes[nodeid]['product'] = nodeinfo.product;
    zwave.nodes[nodeid]['producttype'] = nodeinfo.producttype;
    zwave.nodes[nodeid]['productid'] = nodeinfo.productid;
    zwave.nodes[nodeid]['type'] = nodeinfo.type;
    zwave.nodes[nodeid]['name'] = nodeinfo.name;
    zwave.nodes[nodeid]['loc'] = nodeinfo.loc;
    zwave.nodes[nodeid]['ready'] = true;
    console.log('node%d: %s, %s', nodeid,
        nodeinfo.manufacturer ? nodeinfo.manufacturer : 'id=' + nodeinfo.manufacturerid,
        nodeinfo.product ? nodeinfo.product : 'product=' + nodeinfo.productid +
        ', type=' + nodeinfo.producttype);
    console.log('node%d: name="%s", type="%s", location="%s"', nodeid,
        nodeinfo.name,
        nodeinfo.type,
        nodeinfo.loc);

    for (var comclass in zwave.nodes[nodeid]['classes']) {
        switch (comclass) {
            case 0x25: // COMMAND_CLASS_SWITCH_BINARY
            case 0x26: // COMMAND_CLASS_SWITCH_MULTILEVEL
                zwave.enablePoll(nodeid, comclass);
                break;
        }
        var values = zwave.nodes[nodeid]['classes'][comclass];
        console.log('node%d: class %s', nodeid, comclass);
        for (var idx in values)
            console.log('node%d:   %s=%s', nodeid, values[idx]['label'], values[idx]['value']);
    }
});

zwave.on('notification', function(nodeid, notif) {
    switch (notif) {
        case 0:
            console.log('node%d: message complete', nodeid);
            break;
        case 1:
            console.log('node%d: timeout', nodeid);
            break;
        case 2:
            console.log('node%d: nop', nodeid);
            break;
        case 3:
            console.log('node%d: node awake', nodeid);
            break;
        case 4:
            console.log('node%d: node sleep', nodeid);
            break;
        case 5:
            console.log('node%d: node dead', nodeid);
            break;
        case 6:
            console.log('node%d: node alive', nodeid);
            break;
    }
});

zwave.on('scan complete', function() {
    console.log('zwave: scan complete');
});

zwave.getNode = function(nodeid) {
    var node = zwave.nodes[nodeid];
    if (node) {
        return node;
    } else {
        return new Node();
    }
}

module.exports = zwave;
