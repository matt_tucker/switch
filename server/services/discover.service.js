var Device = require('../models/db-device.model');
var zwave = require('../zwave/zwave')
var _ = require('lodash')



/**
 * Listen to newly added nodes. If the new node is not associated with a device, then
 * create a new device.
 */
zwave.on('node added', function(nodeId) {
    Device.findOne({
        'node_id': nodeId
    }).exec(function(err, device) {
        if (err) {
            return console.error('Error in finding device by for node:' + nodeId, err)
        };
        if (!device) {
            Device.create({
                    type: 'device.zwave.switch',
                    name: 'New Device',
                    node_id: nodeId,
                    location_id: null,
                    schedules: [],
                    image_url: 'assets/images/switch14.svg'
                },
                function(err, devices) {

                });
        }
    })
});
