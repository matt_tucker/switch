'use strict'
var util = require('util');
var Recipe = require('../models/db-recipe.model.js')
var Device = require('../models/db-device.model.js')
var Zwave = require('../zwave/zwave.js')
var _ = require('lodash')

var BaseEventManager = require('../models/base-event-manager.model.js')

function EventManager() {
    BaseEventManager.call(this);
}

EventManager.prototype = Object.create(BaseEventManager.prototype);

EventManager.prototype.constructor = EventManager;



EventManager.prototype.registerRecipeListeners = function() {

    console.log('Register recipe event listeners...')
    var self = this;

    DbRecipe.find({}, function(err, recipes) {
        if (err) return console.error(err);

        //Convert recipes into real recipe models
        var recipes = _.map(recipes, function(recipe) {
            return new Recipe(dbRecipe);
        })

        _.each(recipes, function(recipe) {
            var eventListeners = recipe.getEventListeners()
            self.setListeners(recipe._id, eventListeners)
        })

    })
}



var EventManagerService = new EventManager()

Zwave.on('value changed', function(nodeid, comclass, value) {

    Device.findOne({
        'node_id': nodeid
    }, '_id node_id', function(err, device) {
        if (err) return handleError(err);

        if (!device) {
            console.warn('Device not found for node.')
            return;
        }

        var deviceId = device._id
        var event;

        if (comclass == 0x25) {
            if (value['value']) {
                event = deviceId + "_on"
            } else {
                event = deviceId + "_off"
            }
        }

        if (event) {
            console.log('Event Manager emitting: ', event)
            EventManagerService.emitter.emit(event)
        }
    })

});



module.exports = EventManagerService
