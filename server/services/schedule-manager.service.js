'use strict'
var util = require("util");
var _ = require('lodash')
var later = require('later')
later.date.localTime();

var DbDevice = require('../models/db-device.model');
var Device = require('../models/device.model');
var WeeklySchedule = require('../models/weekly-schedule.model')
var BaseScheduleManager = require('../models/base-schedule-manager.model.js')

var jobsByDevice = {}


function ScheduleManager() {
    BaseScheduleManager.call(this);
}

ScheduleManager.prototype = Object.create(BaseScheduleManager.prototype);

ScheduleManager.prototype.constructor = ScheduleManager;



ScheduleManager.prototype.clearAllDevices = function() {
    //todo: why don't we just get values here directly  ?
    _.each(_.keys(jobsByDevice), function(key) {
        _.each(jobsByDevice[key], function(job) {
            job.clear();
        });
        jobsByDevice[key] = [];
    });
}

ScheduleManager.prototype.scheduleAllDeviceWeeklySchedules = function() {

    console.log('Scheduling device weekly schedules...')
    var self = this;

    DbDevice.find({}, function(err, devices) {
        if (err) return console.error(err);

        //Convert devices to real device models
        var devices = _.map(devices, function(device) {
            return Device.create(device);
        })


        _.each(devices, function(device) {

            self.removeSchedules(device._id)

            _.each(device.weekly_schedules, function(schedule) {
                var weeklySchedule = new WeeklySchedule(schedule);

                self.schedule(device._id, weeklySchedule.getEventListeners())
            })
        })


    })
}

ScheduleManager.prototype.scheduleDeviceWeeklySchedules = function(device) {

    console.log('Scheduling device weekly schedules for '+device._id)
    var self = this;

    self.removeSchedules(device._id)

    _.each(device.weekly_schedules, function(schedule) {
        var weeklySchedule = new WeeklySchedule(schedule);

        self.schedule(device._id, weeklySchedule.getEventListeners())
    })

}

module.exports = new ScheduleManager()
