/**
 * Using Rails-like standard naming convention for endpoints.
 * GET     /devices              ->  index
 * POST    /devices              ->  create
 * GET     /devices/:id          ->  show
 * PUT     /devices/:id          ->  update
 * DELETE  /Devices/:id          ->  destroy
 */

'use strict';

var _ = require('lodash');
var DbDevice = require('../../models/db-device.model');
var zwave = require('../../zwave/zwave');
var ScheduleManager = require('../../services/schedule-manager.service');

// Get list of devices
exports.index = function(req, res) {
    DbDevice.find(function(err, devices) {


        devices = _.map(devices, function(device) {
            var node = zwave.getNode(device.node_id);
            device = device.toObject();
            device.node = node || {};
            device.node._id = device.node_id;
            return device;
        })

        if (err) {
            return handleError(res, err);
        }

        return res.json(200, devices);
    });
};

// Get a single device
exports.show = function(req, res) {

    DbDevice.findById(req.params.id, function(err, device) {
        if (err) {
            return handleError(res, err);
        }
        if (!device) {
            return res.send(404);
        }

        // Add the node from zwave in-memory store to the device. Consider persisting this
        // data in the db.

        device = device.toObject()
        var node = zwave.getNode(device.node_id);
        device.node = node;

        return res.json(device);
    });
};



// Creates a new device in the DB.
exports.create = function(req, res) {
    DbDevice.create(req.body, function(err, device) {
        if (err) {
            return handleError(res, err);
        }
        return res.json(201, device);
    });
};

// Updates an existing device in the DB.
exports.update = function(req, res) {

    var update = req.body;
    console.log('PUT: device update')
    DbDevice.findByIdAndUpdate(req.params.id, {
        $set: update
    }, function(err, device) {
        // console.log('PUT: found device', err, device)
        if (err) return handleError(err);

        var node = zwave.getNode(device.node_id);

        if (_.has(req.query, 'switch')) {
            var switchState = req.query.switch === 'true' ? true : false;
            if (switchState) {
                console.log('server switching on: %d', device.node_id)
                zwave.setValue(device.node_id, 37, 1, 0, true); // turn on 1st relay
                node['classes'][37][0].value=true
            } else {
                console.log('server switching off: %d', device.node_id)
                zwave.setValue(device.node_id, 37, 1, 0, false); // turn on 1st relay
                node['classes'][37][0].value=false
            }

        } else {
            ScheduleManager.scheduleDeviceWeeklySchedules(device)
        }

        //Convert to an object so that we can add non-schema fields.
        var device = device.toObject();
        device.node = node;

        res.json(200, device);
    });


};


// Deletes a device from the DB.
exports.destroy = function(req, res) {
    DbDevice.findById(req.params.id, function(err, device) {
        if (err) {
            return handleError(res, err);
        }
        if (!device) {
            return res.send(404);
        }
        device.remove(function(err) {
            if (err) {
                return handleError(res, err);
            }
            return res.send(204);
        });
    });
};

function handleError(res, err) {
    console.error('Internal server error.', err)
    return res.send(500, err);
}
