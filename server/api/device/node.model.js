'use strict';

var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

var NodeSchema = new Schema({
    id: Number,
    manufacturer: String,
    manufacturerid: String,
    product: String,
    producttype: String,
    productid: String,
    type: String,
    name: String,
    loc: String,
    classes: Object,
    ready: Boolean,
});

module.exports = mongoose.model('Node', NodeSchema);
