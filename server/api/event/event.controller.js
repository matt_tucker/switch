/**
 * Using Rails-like standard naming convention for endpoints.
 * GET     /events              ->  index
 * POST    /events              ->  create
 * GET     /events/:id          ->  show
 * PUT     /events/:id          ->  update
 * DELETE  /events/:id          ->  destroy
 */

'use strict';

var _ = require('lodash');
var EventManager = require('../../services/event-manager.service.js')



// Creates a new location in the DB.
exports.emit = function(req, res) {
    var event = req.params.id
    EventManager.emit(event)
    return res.send(200);
};


function handleError(res, err) {
    return res.send(500, err);
}
