'use strict';

var express = require('express');
var controller = require('./user.controller');
var config = require('../../config/environment');
var auth = require('../../auth/auth.service');

var router = express.Router();

// router.get('/',  controller.index);
// router.delete('/:id', controller.destroy);
// router.get('/me',  controller.me);
// router.put('/:id/password', controller.changePassword);
// router.get('/:id',  controller.show);
// router.put('/:id',  controller.update);
// router.post('/', controller.create);


// router.get('/:id/presence', auth.isAuthenticated(), controller.getPresence)
// router.put('/:id/presence', auth.isAuthenticated(), controller.updatePresence)

router.get('/', auth.hasRole('admin'), controller.index);
router.delete('/:id', auth.hasRole('admin'), controller.destroy);
router.get('/me', auth.isAuthenticated(), controller.me);
router.put('/:id/password', auth.isAuthenticated(), controller.changePassword);
router.get('/:id', auth.isAuthenticated(), controller.show);
router.put('/:id', auth.isAuthenticated(), controller.update);
router.post('/', controller.create);


router.get('/:id/presence', auth.isAuthenticated(), controller.getPresence)
router.put('/:id/presence', auth.isAuthenticated(), controller.updatePresence)

module.exports = router;
