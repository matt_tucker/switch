/**
 * Using Rails-like standard naming convention for endpoints.
 * GET     /recipes              ->  index
 * POST    /recipes              ->  create
 * GET     /recipes/:id          ->  show
 * PUT     /recipes/:id          ->  update
 * DELETE  /recipes/:id          ->  destroy
 */

'use strict';

var _ = require('lodash');
var DbRecipe = require('../../models/db-recipe.model');
var Recipe = require('../../models/recipe.model');
var EventManager = require('../../services/event-manager.service')

// Get list of recipes
exports.index = function(req, res) {
    DbRecipe.find(function(err, recipes) {


        recipes = _.map(recipes, function(recipe) {
            recipe = recipe.toObject();
            return recipe;
        })

        if (err) {
            return handleError(res, err);
        }

        return res.json(200, recipes);
    });
};

// Get a single recipe
exports.show = function(req, res) {

    DbRecipe.findById(req.params.id, function(err, recipe) {
        if (err) {
            return handleError(res, err);
        }
        if (!recipe) {
            return res.send(404);
        }


        return res.json(recipe);
    });
};


// Runa recipe
exports.run = function(req, res) {

    DbRecipe.findById(req.params.id, function(err, doc) {
        if (err) {
            return handleError(res, err);
        }
        if (!doc) {
            return res.send(404);
        }

        var recipe = new Recipe(doc);
        recipe.run()

        return res.json(doc);
    });
};



// Creates a new recipe in the DB.
exports.create = function(req, res) {
    DbRecipe.create(req.body, function(err, recipe) {
        if (err) {
            return handleError(res, err);
        }
        return res.json(201, recipe);
    });
};

// Updates an existing recipe in the DB.
exports.update = function(req, res) {

    var update = req.body;
    var recipeId = req.params.id

    DbRecipe.findByIdAndUpdate(recipeId, {
        $set: update
    }, function(err, dbRecipe) {
        if (err) return handleError(err);

        var  recipe = new Recipe(dbRecipe)

        var eventListeners = recipe.getEventListeners()
        console.log('event listeners:', eventListeners)
        EventManager.setListeners(recipeId, eventListeners)



        //Convert to an object so that we can add non-schema fields.
        var obj = dbRecipe.toObject();


        res.json(200, obj);
    });


};


// Deletes a recipe from the DB.
exports.destroy = function(req, res) {
    DbRecipe.findById(req.params.id, function(err, recipe) {
        if (err) {
            return handleError(res, err);
        }
        if (!recipe) {
            return res.send(404);
        }
        recipe.remove(function(err) {
            if (err) {
                return handleError(res, err);
            }
            return res.send(204);
        });
    });
};

function handleError(res, err) {
    console.error('Internal server error.', err)
    return res.send(500, err);
}
