/**
 * Main application file
 */

'use strict';

var config = require('./config/environment');

var express = require('express');
require('tungus'); // MUST be before requiring mongoose module.
global.TUNGUS_DB_OPTIONS = {
    nativeObjectID: true,
    searchInArray: true
};
var mongoose = require('mongoose');


console.log('================ config', config)

// Connect to database
mongoose.connect(config.mongo.uri, config.mongo.options)



// Setup server
var app = express();
var server = require('http').createServer(app);
require('./config/express')(app);
require('./routes')(app);

//Register models
require('./models/action-manager')
require('./models/condition-manager')
require('./models/device-manager')


if (config.zwave) {
    try {
        var zwave = require('./zwave/zwave')
        require('./services/discover.service')

        zwave.connect('/dev/ttyUSB0');
    } catch (err) {
        console.log('Failed to connnect to zwave controller.')
    }
}

var ScheduleManager = require('./services/schedule-manager.service');

// Populate DB with sample data
if (config.seedDB) {
    console.log('-------------- Seeding database with initial data -------------------')
    require('./config/seed');
} else {
    ScheduleManager.scheduleAllDeviceWeeklySchedules();
}

// Start server
server.listen(config.port, config.ip, function() {
    console.log('Express server listening on %d, in %s mode', config.port, app.get('env'));
});



// var EventManager = require('./services/event-manager.service.js')
// EventManager.addListener('1234', '5548b8ebd5d9ae100727b6f4_on', function() {
//     console.log('wohoo!!! this worked........ON')
// })
// EventManager.addListener('1234', '5548b8ebd5d9ae100727b6f4_off', function() {
//     console.log('wohoo!!! this worked........OFF')
// })


// Expose app
exports = module.exports = app;
