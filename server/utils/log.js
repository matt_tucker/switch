var bunyan = require('bunyan');



var config = {
    name: 'server',
    serializers: {
        req: bunyan.stdSerializers.req,
        res: bunyan.stdSerializers.res
    },
    streams: [{
            path: __dirname + '/../../log/server.log',
        }, {
            stream: process.stdout
        }

    ]
};

var logger = bunyan.createLogger(config);
module.exports = logger;


module.exports.expressLogger = function(req, res, next) {
    logger.info({
        req: req
    }, 'START');
    next();
};


logger.monkeyPatchConsole = function() {
    console.log = function() {
        logger.info.apply(logger, arguments);
    };
    console.error = function() {
        logger.error.apply(logger, arguments);
    };
    console.warn = function() {
        logger.warn.apply(logger, arguments);
    };
    console.info = function() {
        logger.info.apply(logger, arguments);
    };
}


// // Serialize an HTTP request.
// Logger.stdSerializers.req = function req(req) {
//     if (!req || !req.connection)
//         return req;
//     return {
//         method: req.method,
//         url: req.url,
//         headers: req.headers,
//         remoteAddress: req.connection.remoteAddress,
//         remotePort: req.connection.remotePort
//     };
//     // Trailers: Skipping for speed. If you need trailers in your app, then
//     // make a custom serializer.
//     //if (Object.keys(trailers).length > 0) {
//     //  obj.trailers = req.trailers;
//     //}
// };
