'use strict';

var path = require('path');
var _ = require('lodash');
var fs = require('fs');
var log = require('../../utils/log.js');
var nconf = require('nconf');

// Load configurations hierarchicaly by:
//   1. Command-line arguments
//   2. Environment variables
//   3. A file located at '../config.json'
//
nconf.argv()
    .env('__')
    .file({
        file: __dirname + '/../../../config.json'
    });

nconf.defaults({
    environment: 'development',
    debug: true,
    port: 9000,
    seed_db: false,
    root: path.normalize(__dirname + '/../../..'),
    mongo: {
        uri: 'tingodb://./tingodb',
        options: {
            db: {
                safe: true
            }
        }
    },

    secrets: {
        session: 'test-secret'
    },

    facebook: {
        clientID: 'id',
        clientSecret: 'secret',
        callbackURL: '/auth/facebook/callback'
    },

    google: {
        clientID: 'id',
        clientSecret: 'secret',
        callbackURL: '/auth/google/callback'
    }
    ,
    zwave: true
});

var environment = nconf.get('environment')

// If the environment is incorrect we fail instead of masking the problem.
var valid_environments = ['development', 'qa', 'production'];
if (!environment || !_.contains(valid_environments, environment)) {
    throw new Error('environment: ' + environment + ' is not a valid environment. Check your configuration.')
}

// Other modules use the node environment variable so reset it to the nconf
// environment variable.
process.env.NODE_ENV = environment

log.info('Environment (%s)...', environment);

//-- Construct configuration
var Config = {
    env: environment,
    debug: nconf.get('debug'),

    // Server port
    port: nconf.get('port'),

    // Root path of server
    root: nconf.get('root'),

    //If true seed database with data
    seed_db: nconf.get('seed_db'),

    mongo: {
        uri: nconf.get('mongo:uri'),
        options: nconf.get('mongo:options')
    },

    // Secret for session, you will want to change this and make it an environment variable
    secrets: {
        session: nconf.get('secrets:session')
    },

    facebook: {
        clientID: nconf.get('facebook:clientID'),
        clientSecret: nconf.get('facebook:clientSecret'),
        callbackURL: nconf.get('facebook:callbackURL')
    },

    google: {
        clientID: nconf.get('google:clientID'),
        clientSecret: nconf.get('google:clientSecret'),
        callbackURL: nconf.get('google:callbackURL')
    },

    zwave: nconf.get('zwave')

};

module.exports = Config;
