/**
 * Populate DB with sample data on server start
 * to disable, edit config/environment/index.js, and set `seedDB: false`
 */

'use strict';
var mongoose = require('mongoose');
var Location = require('../api/location/location.model');
var User = require('../api/user/user.model');
var DbDevice = require('../models/db-device.model');
var DbRecipe = require('../models/db-recipe.model');
var ScheduleManager = require('../services/schedule-manager.service.js');


if (false) {
    User.find({}).remove(function() {
        User.create({
                provider: 'local',
                role: 'admin',
                name: 'Admin',
                email: 'admin@switch.com',
                password: 'tmppwd'
            },
            function() {
                console.log('finished populating users');
            });
    });
}


if (true) {
    var id = mongoose.Types.ObjectId();

    Location.find({}).remove(function() {
        Location.create({
            name: 'All'
        }, {
            _id: id,
            name: 'Living Room'
        });
    });

    DbDevice.find({}).remove(function() {
        DbDevice.create({
            type: 'device.zwave.switch',
            name: 'Outdoor Lights',
            node_id: 6,
            location_id: null,
            weekly_schedules: [],
            image_url: 'assets/images/christmas186.svg'
        }, {
            type: 'device.zwave.switch',
            name: 'Couch lamp',
            node_id: 7,
            location_id: id,
            weekly_schedules: [],
            image_url: 'assets/images/lamp15.svg'
        }, function(err, devices) {
            ScheduleManager.scheduleAllDeviceWeeklySchedules()
        });
    });
}


var startScript = (function() {/*
                var config = {
                host: 'REDACTED',
                port: 22,
                username: 'REDACTED',
                password: 'REDACTED',
                tryKeyboard: true,
                readyTimeout: 60000,
            }

            var Client = require('ssh2').Client;

            var conn = new Client();
            conn.on('ready', function() {
                // console.log('Client :: ready');
                conn.exec('osascript ~/SpotifyControl/SpotifyControl.scpt play', function(err, stream) {
                    if (err) throw err;
                    stream.on('close', function(code, signal) {
                        // console.log('Stream :: close :: code: ' + code + ', signal: ' + signal);
                        conn.end();
                    }).on('data', function(data) {
                        console.log('STDOUT: ' + data);
                    }).stderr.on('data', function(data) {
                        console.log('STDERR: ' + data);
                    });
                });
            })

            .on('keyboard-interactive',
                    function(name, instructions, instructionsLang, prompts, finish) {
                        finish([config.password]);
                    })
                .connect(config);
    */}).toString().match(/[^]*\/\*([^]*)\*\/\}$/)[1];

var stopScript = (function() {/*
                var config = {
                host: 'REDACTED',
                port: 22,
                username: 'REDACTED',
                password: 'REDACTED',
                tryKeyboard: true,
                readyTimeout: 60000,
            }

            var Client = require('ssh2').Client;

            var conn = new Client();
            conn.on('ready', function() {
                // console.log('Client :: ready');
                conn.exec('osascript ~/SpotifyControl/SpotifyControl.scpt stop', function(err, stream) {
                    if (err) throw err;
                    stream.on('close', function(code, signal) {
                        // console.log('Stream :: close :: code: ' + code + ', signal: ' + signal);
                        conn.end();
                    }).on('data', function(data) {
                        console.log('STDOUT: ' + data);
                    }).stderr.on('data', function(data) {
                        console.log('STDERR: ' + data);
                    });
                });
            })

            .on('keyboard-interactive',
                    function(name, instructions, instructionsLang, prompts, finish) {
                        finish([config.password]);
                    })
                .connect(config);
    */}).toString().match(/[^]*\/\*([^]*)\*\/\}$/)[1];


DbRecipe.find({}).remove(function() {
    DbRecipe.create({
        type: 'recipe',
        name: 'Spotify Stop',
        conditions: [],
        actions: [{
            type: 'action.javascript',
            script: stopScript
        }],
        image_url: 'assets/images/christmas186.svg'
    }, {
        type: 'recipe',
        name: 'Spotify Start',
        conditions: [],
        actions: [{
            type: 'action.javascript',
            script: startScript
        }],
        image_url: 'assets/images/lamp15.svg'
    }, function(err, recipes) {});
});
