/**
 * Main application routes
 */

'use strict';

var errors = require('./components/errors');

var serveIndex = require('serve-index');
var path = require('path');
var config = require('./config/environment');

module.exports = function(app) {

    // CORS (Cross-Origin Resource Sharing) headers to support Cross-site HTTP requests
    app.use(function(req, res, next) {
        res.header("Access-Control-Allow-Origin", "*");
        res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
        res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE');

        res.header('Cache-Control', 'private, no-cache, no-store, must-revalidate');
        res.header('Expires', '-1');
        res.header('Pragma', 'no-cache');
        next();
    });

    app.use('/images', serveIndex(path.join(config.root, 'client/assets/images')));

    // Insert routes below
    app.use('/api/locations', require('./api/location'));
    app.use('/api/users', require('./api/user'));
    app.use('/api/devices', require('./api/device'));
    app.use('/api/recipes', require('./api/recipe'));
    app.use('/api/events', require('./api/event'));

    app.use('/auth', require('./auth'));

    // All undefined asset or api routes should return a 404
    app.route('/:url(api|auth|components|app|bower_components|assets)/*')
        .get(errors[404]);

    // All other routes should redirect to the index.html
    app.route('/*')
        .get(function(req, res) {
            res.sendfile(app.get('appPath') + '/index.html');
        });


};
