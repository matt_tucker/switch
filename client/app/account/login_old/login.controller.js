'use strict';

angular.module('switchApp')
    .controller('LoginCtrl', function($scope, Auth, $location, $window, $modal, $rootScope) {
        $scope.user = {};
        $scope.errors = {};

        $scope.login = function(form) {
            $scope.submitted = true;

            if (form.$valid) {
                Auth.login({
                        email: $scope.user.email,
                        password: $scope.user.password
                    })
                    .then(function() {
                        // Logged in, redirect to home
                        $location.path('/');
                    })
                    .catch(function(err) {
                        $scope.errors.other = err.message;
                    });
            }
        };

        $scope.showit = false;

        $scope.openLogin = function() {
            // console.log('Setting showit to true')
                // $scope.showit = true;

            var loginModal = $modal.open({
                templateUrl: 'components/login/login_modal.html',
                windowClass: 'modal-default',
                // scope: $rootScope.$new(),
                // size:'lg',
                controller: function($scope) {
                    $scope.user = {};
                    $scope.errors = {};
                    $scope.login = function(form) {
                        $scope.submitted = true;

                        if (form.$valid) {
                            Auth.login({
                                    email: $scope.user.email,
                                    password: $scope.user.password
                                })
                                .then(function() {
                                    // Logged in, redirect to home
                                    loginModal.dismiss('cancel');
                                    $location.path('/');
                                })
                                .catch(function(err) {
                                    $scope.errors.other = err.message;
                                });

                        }

                    };

                    $scope.loginOauth = function(provider) {
                        $window.location.href = '/auth/' + provider;
                    };


                    $scope.cancel = function() {
                        loginModal.dismiss('cancel');
                    };
                }
            });
        }


    });



