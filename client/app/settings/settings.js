angular.module('switchApp')
    .config(function($stateProvider) {
        $stateProvider
            .state('settings', {
                abstract: true,
                url: '/settings',
                templateUrl: 'components/sidenav/sidenav-template.html',
                controller: 'SettingsCtrl'
            })
            .state('settings.main', {
                url: '',
                templateUrl: 'app/settings/settings.html',
                authenticate: true
            })
            .state('settings.password', {
                url: '/password',
                templateUrl: 'app/settings/password.html',
                authenticate: true
            })

    });
