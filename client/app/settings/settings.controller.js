'use strict';

angular.module('switchApp')
    .controller('SettingsCtrl', function($scope, $document, $http, $state,
        User, Auth) {
        console.log('Loading settings controller...')
        $scope.errors = {};


        $scope.user = Auth.getCurrentUser();
        $scope.user.preferences = $scope.user.preferences || {}



        $scope.passwordData ={};
        $scope.changePassword = function(form) {
            $scope.submitted = true;
            if (form.$valid) {
                Auth.changePassword($scope.passwordData.oldPassword, $scope.passwordData.newPassword)
                    .then(function() {
                        $scope.message = 'Password successfully changed.';
                    })
                    .catch(function() {
                        form.password.$setValidity('mongoose', false);
                        $scope.errors.other = 'Incorrect password';
                        $scope.message = '';
                    });
            } else {
                console.warn('Form not valid. Not changing password.')
            }
        };

        $scope.savePreferences  = function(newValue, oldValue) {
            $scope.user.$update();
        }

        $scope.$watch('user.preferences.developer_mode', $scope.savePreferences);

        $scope.event = {
            value: ''
        };

        $scope.emitEvent = function() {
            console.log('Emitting myevent:' + $scope.event.value)
            $http.post('/api/events/' + $scope.event.value).

            success(function(data, status, headers, config) {
                console.log(data)
            }).
            error(function(data, status, headers, config) {
                console.log('error', data)
            })
        }


        $scope.toggleDrawer = function() {
            var panel = $document.find("#drawerPanel")[0]
            panel.togglePanel()
        }

        $scope.editPassword = function() {
             $state.go('settings.password');
        }

        $scope.back = function() {
            $state.go('settings.main');
        }
    });
