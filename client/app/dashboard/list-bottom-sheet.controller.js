'use strict';
angular.module('switchApp')
    .controller('ListBottomSheetCtrl', function($scope, $mdBottomSheet) {


        $scope.items = [{
            name: 'Refresh',
            icon: 'navigation:refresh'
        }
        ];
        $scope.listItemClick = function($index) {
            var clickedItem = $scope.items[$index];
            $mdBottomSheet.hide(clickedItem);
        };

    });
