  'use strict';

  angular.module('switchApp')
      .config(function($stateProvider) {
          $stateProvider
              .state("dashboard", {
                  abstract: true,
                  url: "/dashboard",
                  templateUrl: 'components/sidenav/sidenav-template.html',
                  resolve: {
                      locations: function(Location) {
                          return Location.query().$promise;
                      },
                      devices: function(Device) {
                          return Device.query().$promise;
                      },
                      recipes: function(Recipe) {
                          return Recipe.query().$promise;
                      },
                      user: function(Auth) {
                          return Auth.getCurrentUser();
                      }

                  },
                  controller: 'dashboardCtrl'
              })
              .state('dashboard.list', {
                  url: '',
                  abstract: true,
                  templateUrl: 'app/dashboard/dashboard.html'
              })
              .state("dashboard.list.selected", {
                  url: "?selected",
                  authenticate: true
              })
      });
