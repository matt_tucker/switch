'use strict';
angular.module('switchApp')
    .controller('dashboardCtrl', function($scope, $http, $state, $stateParams,
        $location, $interval, $document, $q, $mdBottomSheet, Auth, Device,
        Location, devices, recipes, locations, BackgroundService,
        JavascriptAction, DeviceSwitchAction, JavascriptCondition, Recipe) {

        $scope.isAddVisible = true;

        $scope.data = {
            controls: []
        }

        if ($stateParams.id) {
            $scope.selectedId = $stateParams.id;
        }

        $scope.isSearchVisible = false;

        $scope.search = {
            query: ''
        }

        $scope.toggleSearch = function() {
            $scope.isSearchVisible = !$scope.isSearchVisible

            if (!$scope.isSearchVisible) {
                $scope.search.query = ''
            }
            console.log('Search query = ', $scope.search.query)
        }


        $scope.locations = locations;


        $scope.locationId = 'all';

        $scope.selectLocation = function(locationName, locationId) {

            console.log('Selecting location: ', locationName, locationId)


            $scope.controls = devices.concat(recipes);

            if (locationName.toLowerCase() === 'all') {
                $scope.locationId = 'all'
                console.log('devices/recipes', devices, recipes, $scope.controls)
            } else {
                $scope.locationId = locationId;

                $scope.controls = _.filter($scope.controls, function(control) {
                    return control.location_id == locationId;
                })
            }

        }


        $interval(function() {
            console.log('Updating controls...')
            Device.query(function(devices) {
                console.log('Updating devices...query complete')

                if ($scope.locationId === 'all') {
                    $scope.updated_controls = devices.concat(recipes);

                    _.each(devices, function(device) {
                        console.log('device:', device)
                    })

                } else {

                    var newDevices = _.filter(devices, function(device) {
                        return device.location_id == $scope.locationId;
                    })


                    $scope.updated_controls = newDevices.concat(recipes);
                }
                // $scope.$apply();
                //Don't update the device being edited. Its frozen.
                // $scope.device = _.findWhere($scope.devices, {
                //     _id: $state.params.id
                // });

                _.each($scope.updated_controls, function(update) {
                    $scope.control = _.findWhere($scope.controls, {
                        _id: update._id
                    });
                    if ($scope.control) {
                        $scope.control = update
                    }
                })

                console.log('Updating controls...COMPLETE')
            });

        }, 10000);


        $scope.isSelected = function(partner) {
            var selected = false;
            if ($stateParams.id) {
                selected = partner._id == $stateParams.id;
            }
            return selected;
        }

        $scope.sortableOptions = {
            axis: 'y',
            handle: '.gripper',
            update: function(e, ui) {
                var partner = ui.item.scope().partner;
            },
            stop: function(e, ui) {
                // _.each(scope.devices, updateIndex);
            }
        };

        $scope.edit = function(item) {

            console.log('-% edit item %')

            var currentState = $state.current.name;

            if (!item) {
                item = {
                    '_id': 'new'
                };
            }



            var baseType = item.getBaseType() + 's'



            var view = $stateParams.view || 'info';
            $state.go(baseType + '.detail', {
                'id': item._id,
                'view': view
            })


        };

        $scope.toggleDrawer = function() {
            var panel = $document.find("#drawerPanel")[0]
            panel.togglePanel()
        }


        $scope.addRecipe = function() {
            var actions = []
            actions.push(new JavascriptAction())
            actions.push(new DeviceSwitchAction())
            var condition = new JavascriptCondition()
            var recipe = new Recipe({
                type: 'recipe',
                name: 'New recipe',
                image_url: 'assets/images/switch14.svg',
                conditions: [condition],
                actions: actions
            });

            recipe.$save()
            $scope.controls.push(recipe)
        }
        $scope.addGroup = function() {
            var actions = []
            actions.push(new JavascriptAction())
            actions.push(new DeviceSwitchAction())
            var condition = new JavascriptCondition()
            var recipe = new Recipe({
                type: 'recipe',
                name: 'New recipe',
                image_url: 'assets/images/switch14.svg',
                conditions: [condition],
                actions: actions
            });

            recipe.$save()
            $scope.controls.push(recipe)
        }



        $scope.add = function() {
            $scope.addRecipe()
        }



        //
        // Select all locations
        //
        $scope.selectLocation('all')


        $scope.showListBottomSheet = function($event) {
            console.log('mkt show list bottom sheet')
            $scope.alert = '';
            $mdBottomSheet.show({
                templateUrl: '/app/dashboard/list-bottom-sheet.html',
                controller: 'ListBottomSheetCtrl',
                targetEvent: $event
            }).then(function(clickedItem) {
                $scope.alert = clickedItem['name'] + ' clicked!';
            });
        };


    });
