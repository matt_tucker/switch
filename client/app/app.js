'use strict';

angular.module('switchApp', [
    'ngCookies',
    'ngResource',
    'ngSanitize',
    'ngMaterial',
    'ui.router',
    'ui.bootstrap',
    // 'ng-polymer-elements',
    'ui.ace'
])

.config(function($stateProvider, $urlRouterProvider, $locationProvider,
    $httpProvider, $provide, $mdIconProvider) {
    $urlRouterProvider.otherwise('/dashboard');

    $locationProvider.html5Mode(true);
    $httpProvider.interceptors.push('authInterceptor');

    $provide.decorator('$state', function($delegate, $stateParams) {
        $delegate.forceReload = function() {
            console.log('Reloading state ' + $delegate.current.name)
            return $delegate.go($delegate.current.name, $stateParams, {
                reload: true,
                inherit: false,
                notify: true
            });
        };
        return $delegate;
    });

    // $mdIconProvider
    //     .iconSet("call", 'img/icons/sets/communication-icons.svg', 24)
    //     .iconSet("social", 'img/icons/sets/social-icons.svg', 24);



})

.config(['$mdIconProvider', function($mdIconProvider) {
    $mdIconProvider
        .iconSet('action', '/img/icons/material-design/action-icons.svg', 24)
        .iconSet('alert', '/img/icons/material-design/alert-icons.svg', 24)
        .iconSet('av', '/img/icons/material-design/av-icons.svg', 24)
        .iconSet('communication', '/img/icons/material-design/communication-icons.svg', 24)
        .iconSet('content', '/img/icons/material-design/content-icons.svg', 24)
        .iconSet('device', '/img/icons/material-design/device-icons.svg', 24)
        .iconSet('editor', '/img/icons/material-design/editor-icons.svg', 24)
        .iconSet('file', '/img/icons/material-design/file-icons.svg', 24)
        .iconSet('hardware', '/img/icons/material-design/hardware-icons.svg', 24)
        .iconSet('icons', '/img/icons/material-design/icons-icons.svg', 24)
        .iconSet('image', '/img/icons/material-design/image-icons.svg', 24)
        .iconSet('maps', '/img/icons/material-design/maps-icons.svg', 24)
        .iconSet('mdi', '/img/icons/material-design/mdi-icons.svg', 24)
        .iconSet('navigation', '/img/icons/material-design/navigation-icons.svg', 24)
        .iconSet('notification', '/img/icons/material-design/notification-icons.svg', 24)
        .iconSet('social', '/img/icons/material-design/social-icons.svg', 24)
        .iconSet('toggle', '/img/icons/material-design/toggle-icons.svg', 24)
}])

.config(function($mdThemingProvider) {
    // var customBlueMap = $mdThemingProvider.extendPalette('light-blue', {
    //     'contrastDefaultColor': 'light',
    //     'contrastDarkColors': ['50'],
    //     '50': 'ffffff'
    // });
    // $mdThemingProvider.definePalette('customBlue', customBlueMap);
    // $mdThemingProvider.theme('default')
    //     .primaryPalette('customBlue', {
    //         'default': '500',
    //         'hue-1': '50'
    //     })
    //     .accentPalette('pink');
    // $mdThemingProvider.theme('input', 'default')
    //     .primaryPalette('grey')
})

.factory('authInterceptor', function($rootScope, $q, $cookieStore, $location) {
    return {
        // Add authorization token to headers
        request: function(config) {
            config.headers = config.headers || {};
            if ($cookieStore.get('token')) {
                config.headers.Authorization = 'Bearer ' + $cookieStore.get('token');
            }
            return config;
        },

        // Intercept 401s and redirect you to login
        responseError: function(response) {
            console.log('401 go to login')
            if (response.status === 401) {
                $location.path('/login');
                // remove any stale tokens
                $cookieStore.remove('token');
                return $q.reject(response);
            } else {
                return $q.reject(response);
            }
        }
    };
})


.directive('shakeThat', ['$animate', function($animate) {
    return {
        require: '^form',
        scope: {
            submit: '&',
            submitted: '='
        },
        link: function(scope, element, attrs, form) {

            // listen on submit event
            element.on('submit', function() {

                var shake = function(element) {
                    $animate.addClass(element, 'shake').then(function() {
                        $animate.removeClass(element, 'shake');
                    })
                }

                // tell angular to update scope
                scope.$apply(function() {
                    // everything ok -> call submit fn from controller
                    if (!form.$valid) {
                        // show error messages on submit
                        scope.submitted = true;
                        shake(element)
                        return;
                    }

                    return scope.submit().catch(function() {
                        shake(element)
                    })

                });
            });
        }
    };

}])

.run(function($rootScope, $location, Auth) {
    // Redirect to login if route requires auth and you're not logged in
    $rootScope.$on('$stateChangeStart', function(event, next) {
        Auth.isLoggedInAsync(function(loggedIn) {
            if (next.authenticate && !loggedIn) {
                $location.path('/login');
            }
        });
    });
});
