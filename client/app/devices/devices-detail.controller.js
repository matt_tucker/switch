'use strict';

angular.module('switchApp')
    .controller('devicesDetailCtrl', function($scope, $http, $state, $stateParams,
        $modal, $resource, $interval, $window, $mdToast, Device, WeeklySchedule, ScheduleCondition, SolarCondition,
        DeviceRecipe, DeviceSwitchAction, user) {




        later.date.localTime();


        $scope.date = new Date();

        $interval(function() {
            $scope.date = new Date();
        }, 1000);



        // $scope.device = angular.copy(_.findWhere($scope.devices, {
        //     _id: $stateParams.id
        // }));

        $scope.device = Device.get({
            id: $stateParams.id
        }, function(device) {

            sortSchedules()
        })

        console.log('device', $scope.device)

        function sortSchedules() {
            console.log('--------------Sorting schedules  START --------------')
            try {
                _.each($scope.device.weekly_schedules, function(schedule) {

                    console.log('sorting recipes')
                    schedule.recipes = _.sortBy(schedule.recipes, function(recipe) {

                        var condition = recipe.conditions[0]

                        if (condition.type === 'condition.schedule') {
                            console.log('creating new later condition')
                            condition = new ScheduleCondition(condition)
                        } else if (condition.type === 'condition.solar') {
                            condition = new SolarCondition(condition)
                            console.log('sun condition event:', condition.event)
                        } else {
                            console.warn('Unrecognized condition: ', condition.type)
                        }


                        var schedule = condition.getSchedule();

                        var nextOccurrence = later.schedule(schedule).next()

                        console.log('next occurence:', schedule, nextOccurrence)

                        return nextOccurrence.getTime();
                    });
                })
            } catch (err) {
                console.log('Failed to sort schedules: ', err)
            }

            console.log('--------------Sorting schedules  END --------------')
        }





        var currentState = $state.current.name;
        var parentState = '';
        var index = currentState.lastIndexOf('.')
        if (index > 0) {
            parentState = currentState.substr(0, index);
        }



        $scope.go = function(route) {
            var view = 'info';
            var index = route.lastIndexOf('.') + 1;
            if (index > 0) {
                view = route.substr(index);
            }
            console.log('view = ', view)

            // $state.go(route);

            //Need to explicitly set the view so the url is updated.
            $state.go(route, {
                'view': view
            });
        };

        $scope.back = function back() {
            $state.go('dashboard.list.selected');
        }
        $scope.active = function(route) {
            return $state.is(route);
        };


        /**
         * Send device to server to be saved.
         */
        $scope.save = function() {

            $scope.device.$update(function(savedDevice) {
                var message = 'Device saved successfully.'
                console.log(message)

                $mdToast.show(
                    $mdToast.simple()
                    .content(message)
                    .hideDelay(2000)
                );

                var index = _.findIndex($scope.devices, {
                    _id: $stateParams.id
                });
                if (index) {
                    $scope.devices[index] = $scope.device;
                }
            });
        }


        $scope.delete = function() {

            var title = 'Delete  ' + $scope.device.name + ' ?'
            var parentScope = $scope;

            var modal = $modal.open({
                templateUrl: 'components/modal/confirm_modal.html',
                controller: function($scope) {
                    $scope.title = title;
                    $scope.okLabel = "Delete";
                    $scope.cancelLabel = "Cancel";


                    $scope.ok = function() {
                        var deviceId = parentScope.device._id
                        parentScope.device.$delete(function(device) {

                            // parentScope.closeDetailPanel()
                            parentScope.devices = _.filter(parentScope.devices, function(device) {
                                return device._id != deviceId;
                            })

                            var message = 'Device deleted successfully.'
                            console.log(message)
                            $mdToast.show(
                                $mdToast.simple()
                                .content(message)
                                .hideDelay(2000)
                            );
                            modal.dismiss('cancel');
                        })
                    };

                    $scope.cancel = function() {
                        modal.dismiss('cancel');
                    };
                }
            });


        }



        $scope.chooseImage = function() {

            var title = 'Select Image:'
            var parentScope = $scope;

            var modal = $modal.open({
                templateUrl: 'app/devices/choose-image-modal.html',
                controller: function($scope) {
                    $scope.title = title;
                    $scope.okLabel = "OK";
                    $scope.cancelLabel = "Cancel";

                    $scope.imageUrl = parentScope.device.image_url
                    $scope.images = $resource('/images').query();

                    $scope.getImageUrl = function(filename) {
                        return 'assets/images/' + filename
                    }

                    $scope.isSelected = function(image) {
                        return $scope.getImageUrl(image) === $scope.imageUrl
                    }

                    $scope.selectImage = function(image) {

                        $scope.imageUrl = $scope.getImageUrl(image)
                        console.log('Selecting ' + $scope.imageUrl)
                    }

                    $scope.ok = function() {
                        parentScope.device.image_url = $scope.imageUrl;
                        modal.dismiss('cancel');
                    };

                    $scope.cancel = function() {
                        modal.dismiss('cancel');
                    };
                }
            });


        }

        $scope.addSchedule = function() {

            var weeklySchedule = new WeeklySchedule([1, 2, 3, 4, 5, 6, 7])


            var recipe = new DeviceRecipe();
            var schedule = {
                schedules: [{
                    h: [8],
                    m: [0]
                }]
            }
            recipe.conditions.push(new ScheduleCondition({
                schedule: schedule
            }))
            recipe.actions.push(new DeviceSwitchAction($scope.device._id, false))

            weeklySchedule.recipes.push(recipe)
            $scope.device.weekly_schedules.push(weeklySchedule)


            // processSchedules();
        }



        $scope.getJsonString = function(obj) {
            return JSON.stringify(obj, null, 2);
        }

        $scope.locationChanged = function() {
            console.log('device:', $scope.device.location_id, $scope.locations)
        }
    });
