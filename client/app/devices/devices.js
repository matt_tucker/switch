  'use strict';

  angular.module('switchApp')
      .config(function($stateProvider) {
          $stateProvider
              .state("devices", {
                  abstract: true,
                  url: "/devices",
                  templateUrl: 'components/sidenav/sidenav-template.html',
                  resolve: {
                      locations: function(Location) {
                          return Location.query();
                      },
                      devices: function(Device) {
                          return Device.query();
                      },
                      user: function(Auth) {
                          return Auth.getCurrentUser();
                      }
                  },
                  controller: 'devicesCtrl'
              })
              .state('devices.list', {
                  url: '',
                  abstract: true,
                  templateUrl: 'app/devices/devices-list.html'
              })
              .state("devices.list.selected", {
                  url: "?selected",
                  authenticate: true
              })
              .state("devices.detail", {
                  url: '/:id?tab',
                  controller: 'devicesDetailCtrl',
                  templateUrl: 'app/devices/devices-detail.html',
                  reloadOnSearch: false
              })


      });
