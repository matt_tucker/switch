'use strict';
console.log('Loading devices_controller.js')
angular.module('switchApp')
    .controller('devicesCtrl', function($scope, $http, $state, $stateParams,
        $location, $interval, $document, $q, Auth, Device, Location, devices,
        locations, BackgroundService, JavascriptAction, DeviceSwitchAction,
        JavascriptCondition, Recipe) {


        if ($stateParams.id) {
            $scope.selectedId = $stateParams.id;
        }

        $scope.isSearchVisible = false;

        $scope.search = {
            query: ''
        }
        $scope.toggleSearch = function() {
            $scope.isSearchVisible = !$scope.isSearchVisible

            if (!$scope.isSearchVisible) {
                $scope.search.query = ''
            }
            console.log('Search query = ', $scope.search.query)
        }


        $scope.locations = locations;


        $scope.locationId = 'all';

        $scope.selectLocation = function(locationName, locationId) {

            console.log('Selecting location: ', locationName, locationId)
            $q.all([devices.$promise]).then(function() {

                $scope.devices = devices

                if (locationName.toLowerCase() === 'all') {
                    $scope.locationId = 'all'
                } else {
                    $scope.locationId = locationId;

                    $scope.devices = _.filter($scope.devices, function(device) {
                        return device.location_id == locationId;
                    })

                }
            })
        }


        // $interval(function() {
        //     console.log('Updating devices...')
        //     Device.query(function(devices) {
        //         console.log('Updating devices...query complete')

        //         if ($scope.locationId === 'all') {
        //             $scope.devices = devices;
        //             console.log(devices, $scope.devices.length)

        //         } else {

        //             var newDevices = _.filter(devices, function(device) {
        //                 console.log('device', device)
        //                 return device.location_id == $scope.locationId;
        //             })


        //             $scope.devices = newDevices;
        //         }

        //         //Don't update the device being edited. Its frozen.
        //         // $scope.device = _.findWhere($scope.devices, {
        //         //     _id: $state.params.id
        //         // });

        //         console.log('Updating devices...COMPLETE')
        //     });

        // }, 10000);


        $scope.isSelected = function(partner) {
            var selected = false;
            if ($stateParams.id) {
                selected = partner._id == $stateParams.id;
            }
            return selected;
        }

        $scope.sortableOptions = {
            axis: 'y',
            handle: '.gripper',
            update: function(e, ui) {
                var partner = ui.item.scope().partner;
            },
            stop: function(e, ui) {
                // _.each(scope.devices, updateIndex);
            }
        };

        $scope.edit = function(item) {

            console.log('-% edit item %')

            var currentState = $state.current.name;

            if (!item) {
                item = {
                    '_id': 'new'
                };
            }


            var view = $stateParams.view || 'info';
            $state.go('devices.detail', {
                'id': item._id,
                'view': view
            })


        };


        $scope.selectLocation('all')

        $scope.toggleDrawer = function() {
            var panel = $document.find("#drawerPanel")[0]
            panel.togglePanel()
        }

    });
