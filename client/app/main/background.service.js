'use strict';

angular.module('switchApp')
    .factory('BackgroundService', function() {
        var currentBackgroundClass = 'home-bg';
        return {
            setCurrentBg: function(aClass) {
                currentBackgroundClass = aClass;
            },
            getCurrentBg: function() {
                return currentBackgroundClass;
            }
        };
    });
