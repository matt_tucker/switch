'use strict';

angular.module('switchApp')
    .controller('MainCtrl', function($scope, $http, $state, $stateParams,
        $mdSidenav, $location, Device, Auth) {

        $scope.isCollapsed = true;
        $scope.isLoggedIn = Auth.isLoggedIn;
        $scope.isAdmin = Auth.isAdmin;
        $scope.getCurrentUser = Auth.getCurrentUser;

        $scope.user = Auth.getCurrentUser()

        $scope.logout = function() {
            console.log('Signing out...')
            Auth.logout();
            $location.path('/login');
        };

        $scope.isActive = function(route) {
            return route === $location.path();
        };

        //
        // Side Navigation
        //

        $scope.toggleSidenav = function(menuId) {
            $mdSidenav(menuId).toggle();
        };

        // Menu items
        $scope.menu = [{
                link: function() {
                    $state.go('dashboard.list.selected')
                },
                title: 'Dashboard',
                icon: 'action:dashboard'
            }

        ];
        $scope.admin = [
        {
            link: function() {
                $state.go('users.list')
            },
            title: 'Users',
            icon: 'action:settings'
        },

        {
            link: function() {
                $state.go('settings.main')
            },
            title: 'Settings',
            icon: 'action:settings'
        },




        {
            link: $scope.logout,
            title: 'Logout',
            icon: 'action:account_box'
        }, ];

    });
