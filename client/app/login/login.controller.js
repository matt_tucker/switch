'use strict';

angular.module('switchApp')
    .controller('LoginCtrl', function($scope, $http, $state, $stateParams, $location,
        $animate,$q, BackgroundService, Auth) {
        BackgroundService.setCurrentBg('home-bg')
        $scope.user = {};
        $scope.errors = {};

        // if (Auth.isLoggedIn()) {
        //      $state.go('dashboard.list.selected')
        // }


        $scope.login = function(form) {
            $scope.submitted = true;

            if (form.$valid) {
               return Auth.login({
                        email: $scope.user.email,
                        password: $scope.user.password
                    })
                    .then(function() {
                        // Logged in, redirect to home
                        console.log('Logged in successfully.')
                        $location.path('/dashboard');
                    })
                    .catch(function(err) {
                        $scope.errors.other = err.message;
                        return $q.reject(err)
                    });

            }

        };

        $scope.loginOauth = function(provider) {
            $window.location.href = '/auth/' + provider;
        };
    });
