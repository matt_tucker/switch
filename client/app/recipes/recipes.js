  'use strict';

  angular.module('switchApp')
      .config(function($stateProvider) {
          $stateProvider
              .state("recipes", {
                  abstract: true,
                  url: "/recipes",
                  templateUrl: 'components/sidenav/sidenav-template.html',
                  resolve: {
                      recipes: function(Recipe) {
                          return Recipe.query();
                      }

                  },
                  controller: 'recipesCtrl'
              })
              .state('recipes.list', {
                  url: '',
                  abstract: true,
                  templateUrl: 'app/recipes/recipes-list.html'
              })
              .state("recipes.list.selected", {
                  url: "?selected",
                  authenticate: true
              })
              .state("recipes.detail", {
                  url: '/:id?tab',
                  controller: 'recipesDetailCtrl',
                  templateUrl: 'app/recipes/recipes-detail.html',
                  reloadOnSearch: false
              })
      });
