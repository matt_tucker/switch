'use strict';

angular.module('switchApp')
    .controller('recipesDetailCtrl', function($scope, $http, $state, $stateParams,
        $modal, $resource, $location, $window, $mdToast, Recipe, DeviceSwitchAction,
        EventCondition) {

        console.log("mkt recipes detail controller ")

        $scope.recipe = Recipe.get({
            id: $stateParams.id
        }, function() {})


        var currentState = $state.current.name;
        var parentState = '';
        var index = currentState.lastIndexOf('.')
        if (index > 0) {
            parentState = currentState.substr(0, index);
        }


        $scope.selectedTab = _.findIndex($scope.tabs, function(tab) {
            return tab.route === $state.current.name
        })

        $scope.go = function(route) {
            var view = 'info';
            var index = route.lastIndexOf('.') + 1;
            if (index > 0) {
                view = route.substr(index);
            }


            //Need to explicitly set the view so the url is updated.
            $state.go(route, {
                'view': view
            });
        };

        $scope.back = function back() {
            $state.go('dashboard.list.selected');
        }

        $scope.active = function(route) {
            return $state.is(route);
        };

        $scope.$on("$stateChangeSuccess", function() {
            $scope.tabs.forEach(function(tab) {
                tab.active = $scope.active(tab.route);
            });
        });



        /**
         * Send recipe to server to be saved.
         */
        $scope.save = function() {
            $scope.recipe.$update(function(savedRecipe) {

                var message = 'Recipe saved successfully.'
                console.log(message)
                $mdToast.show(
                    $mdToast.simple()
                    .content(message)
                    .hideDelay(2000)
                );

                var index = _.findIndex($scope.recipes, {
                    _id: $stateParams.id
                });
                if (index) {
                    $scope.recipes[index] = $scope.recipe;
                }
            });
        }


        $scope.delete = function() {

            var title = 'Delete  ' + $scope.recipe.name + ' ?'
            var parentScope = $scope;

            var modal = $modal.open({
                templateUrl: 'components/modal/confirm_modal.html',
                controller: function($scope) {
                    $scope.title = title;
                    $scope.okLabel = "Delete";
                    $scope.cancelLabel = "Cancel";


                    $scope.ok = function() {
                        var recipe_id = parentScope.recipe._id
                        parentScope.recipe.$delete(function(recipe) {


                            var recipes = _.filter(parentScope.recipes, function(recipe) {
                                return recipe._id != recipe_id;
                            })
                            parentScope.updateRecipes(recipes);
                            modal.dismiss('cancel');
                            parentScope.back()

                            var message = 'Recipe deleted successfully.'
                            console.log(message)
                            $mdToast.show(
                                $mdToast.simple()
                                .content(message)
                                .hideDelay(2000)
                            );

                        })
                    };

                    $scope.cancel = function() {
                        modal.dismiss('cancel');
                    };
                }
            });


        }



        $scope.chooseImage = function() {

            var title = 'Select Image:'
            var parentScope = $scope;

            var modal = $modal.open({
                templateUrl: 'app/recipes/choose-image-modal.html',
                controller: function($scope) {
                    $scope.title = title;
                    $scope.okLabel = "OK";
                    $scope.cancelLabel = "Cancel";

                    $scope.imageUrl = parentScope.recipe.image_url
                    $scope.images = $resource('/images').query();

                    $scope.getImageUrl = function(filename) {
                        return 'assets/images/' + filename
                    }

                    $scope.isSelected = function(image) {
                        return $scope.getImageUrl(image) === $scope.imageUrl
                    }

                    $scope.selectImage = function(image) {

                        $scope.imageUrl = $scope.getImageUrl(image)
                        console.log('Selecting ' + $scope.imageUrl)
                    }

                    $scope.ok = function() {
                        parentScope.recipe.image_url = $scope.imageUrl;
                        modal.dismiss('cancel');
                    };

                    $scope.cancel = function() {
                        modal.dismiss('cancel');
                    };
                }
            });


        }

        $scope.safeApply = function(fn) {
            var phase = this.$root.$$phase;
            if (phase == '$apply' || phase == '$digest') {
                if (fn && (typeof(fn) === 'function')) {
                    fn();
                }
            } else {
                this.$apply(fn);
            }
        };

        $scope.getJsonString = function(obj) {
            return JSON.stringify(obj, null, 2);
        }

        $scope.addAction = function() {
            $scope.recipe.actions.push(new DeviceSwitchAction())
        }
        $scope.addCondition = function(conditionType) {

            switch (conditionType) {
                case 'condition.event':
                    var condition = new EventCondition();
                    condition.sourceId = '554f6312b01b83502e335ea3';
                    condition.sourceType = 'switch';
                    condition.event = 'on';
                    break;
                default:
                    console.warn('Unable to add contion. Condition not recognized.' + conditionType);
                    return;
            }


            $scope.recipe.conditions.push(condition)
        }
    });
