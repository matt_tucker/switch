'use strict';
console.log('Loading recipes.controller.js')
angular.module('switchApp')
    .controller('recipesCtrl', function($scope, $http, $state, $stateParams,
        $location, $interval, $document, Auth, Recipe, recipes,
        JavascriptCondition, JavascriptAction) {

        console.log('recipes.controller Recipes :', recipes)
        $scope.recipes = recipes

        if ($stateParams.id) {
            $scope.selectedId = $stateParams.id;
        }


        $scope.edit = function(item) {

            var currentState = $state.current.name;

            if (!item) {
                item = {
                    '_id': 'new'
                };
            }

            var view = $stateParams.view || 'info';
            $state.go('recipes.detail.' + view, {
                'id': item._id,
                'view': view
            })

        };

        $scope.toggleDrawer = function() {
            var panel = $document.find("#drawerPanel")[0]
            panel.togglePanel()
        }


        $scope.addRecipe = function() {
            var actions = []
            actions.push(new JavascriptAction())
            actions.push(new DeviceSwitchAction())
            var condition = new JavascriptCondition()
            var recipe = new Recipe({
                type: 'recipe',
                name: 'New recipe',
                image_url: 'assets/images/switch14.svg',
                conditions: [condition],
                actions: actions
            });

            recipe.$save()
            $scope.recipes.push(recipe)
        }

        $scope.back = function back() {
            $state.go('recipes.list.selected');
        }

        $scope.updateRecipes = function(recipes) {
            $scope.recipes = recipes;
        };

    });
