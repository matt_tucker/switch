'use strict';

angular.module('switchApp')
  .controller('UsersCtrl', function ($scope, $http, Auth, User, users) {

    // Use the User $resource to fetch all users
    // $scope.users = User.query();
    $scope.users = users;

    $scope.delete = function(user) {
      User.remove({ id: user._id });
      angular.forEach($scope.users, function(u, i) {
        if (u === user) {
          $scope.users.splice(i, 1);
        }
      });
    };
  });
