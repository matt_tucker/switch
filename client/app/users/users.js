'use strict';

angular.module('switchApp')
  .config(function ($stateProvider) {
    $stateProvider

      .state("users", {
                  abstract: true,
                  url: "/users",
                  templateUrl: 'components/sidenav/sidenav-template.html',
                  resolve: {
                      users: function(User) {
                          return User.query();
                      }

                  },
                  controller: 'UsersCtrl'
              })
              .state('users.list', {
                  url: '',
                  templateUrl: 'app/users/users-list.html'
              })
  });
