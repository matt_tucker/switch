'use strict';

angular.module('switchApp')
    .controller('NavbarCtrl', function($scope, $location, $modal, $window, Auth) {

        $scope.isCollapsed = true;
        $scope.isLoggedIn = Auth.isLoggedIn;
        $scope.isAdmin = Auth.isAdmin;
        $scope.getCurrentUser = Auth.getCurrentUser;

        $scope.logout = function() {
            console.log('Signing out...')
            Auth.logout();
            $location.path('/login');
        };

        $scope.isActive = function(route) {
            return route === $location.path();
        };

        $scope.openLogin = function() {
            // console.log('Setting showit to true')
            // $scope.showit = true;

            var loginModal = $modal.open({
                templateUrl: 'components/login/login_modal.html',
                windowClass: 'modal-default',
                // scope: $rootScope.$new(),
                // size:'lg',
                controller: function($scope) {
                    $scope.user = {};
                    $scope.errors = {};
                    $scope.login = function(form) {
                        $scope.submitted = true;

                        if (form.$valid) {
                            Auth.login({
                                    email: $scope.user.email,
                                    password: $scope.user.password
                                })
                                .then(function() {
                                    // Logged in, redirect to home
                                    loginModal.dismiss('cancel');
                                    $location.path('/devices');
                                })
                                .catch(function(err) {
                                    $scope.errors.other = err.message;
                                });

                        }

                    };

                    $scope.loginOauth = function(provider) {
                        $window.location.href = '/auth/' + provider;
                    };


                    $scope.cancel = function() {
                        loginModal.dismiss('cancel');
                    };
                }
            });
        }
    });
