'use strict';

/**
 * schedule
 */
angular.module('switchApp')
    .directive('schedule', function($modal, Device, DeviceRecipe, ScheduleCondition,
        SolarCondition, DeviceSwitchAction) {
        return {
            restrict: 'E',
            scope: {
                schedule: '=schedule',
                scheduleIndex: '=scheduleIndex',
                device: '=device'
            },
            templateUrl: 'components/schedule/weekly-schedule.html',
            link: function(scope, el, attrs) {

                scope.ScheduleCondition = ScheduleCondition

                scope.DOW_LABELS = ['Mon', 'Tue', 'Wed', 'Thur', 'Fri', 'Sat', 'Sun']
                scope.dows = [false, false, false, false, false, false, false];

                _.each(scope.schedule.dows, function(dow) {
                    //LATER dows range from 1-7 where 1=sunday
                    var index = dow - 2
                    if (index < 0) {
                        index = 6
                    }
                    scope.dows[index] = true;
                })



                scope.$watch('dows', function(newValue, oldValue) {
                    console.log('Setting dows...',scope.dows, newValue)
                    var dows = [];
                    for (var i = 0; i < scope.dows.length; i++) {

                        // 0=Mon....6=Sun
                        // to 1=Sun, 2=Mon,...7=Sat
                        var index = i + 2; //0-6 -> 2-8
                        if (index == 8) {
                            index = 1;
                        }
                        if (scope.dows[i]) {
                            dows.push(index);
                        }
                    }
                    dows = dows.sort()
                    scope.schedule.dows = dows;

                    // update condition schedules
                    _.each(scope.schedule.recipes, function(recipe) {
                        _.each(recipe.conditions, function(condition) {

                            if (condition.type == 'condition.schedule') {
                                condition.schedule.schedules[0]['dw'] = dows
                            } else if (condition.type == 'condition.solar') {
                                condition.dows = dows
                            }
                        })
                    })
                    console.log('new schedule', scope.schedule)
                    console.log('device', scope.device)
                }, true);


                scope.addRecipe = function(conditionType) {
                    var index = scope.schedule.recipes.length - 1;

                    var recipe = new DeviceRecipe();

                    if (conditionType == 'condition.schedule') {
                        var condition = {
                            schedule: {
                                schedules: [{
                                    h: [8],
                                    m: [0],
                                    dw: scope.schedule.dows
                                }]
                            }
                        }
                        recipe.conditions.push(new ScheduleCondition(condition))
                    } else if (conditionType == 'condition.solar') {
                        recipe.conditions.push(new SolarCondition())
                    }

                    recipe.actions.push(new DeviceSwitchAction(scope.device._id, false))

                    scope.schedule.recipes.splice(index + 1, 0, recipe);
                }

                scope.removeRule = function(index) {
                    var title = 'Remove rule from weekly schedule?'

                    var modal = $modal.open({
                        templateUrl: 'components/modal/confirm_modal.html',
                        controller: function($scope) {
                            $scope.title = title;
                            $scope.okLabel = "Remove";
                            $scope.cancelLabel = "Cancel";

                            $scope.ok = function() {
                                scope.schedule.recipes.splice(index, 1);

                                modal.dismiss('cancel');
                            };

                            $scope.cancel = function() {
                                modal.dismiss('cancel');
                            };
                        }
                    });
                }

                scope.removeSchedule = function() {
                    var title = 'Remove schedule ' + scope.scheduleIndex + ' ?'

                    var modal = $modal.open({
                        templateUrl: 'components/modal/confirm_modal.html',
                        controller: function($scope) {
                            $scope.title = title;
                            $scope.okLabel = "Remove";
                            $scope.cancelLabel = "Cancel";

                            $scope.ok = function() {
                                scope.device.weekly_schedules.splice(scope.scheduleIndex, 1);

                                modal.dismiss('cancel');
                            };

                            $scope.cancel = function() {
                                modal.dismiss('cancel');
                            };
                        }
                    });
                }


            }
        };
    });
