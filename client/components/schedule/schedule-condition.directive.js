'use strict';

/**
 * schedule
 */
angular.module('switchApp')
    .directive('scheduleCondition', function($modal, ScheduleCondition) {
        return {
            restrict: 'E',
            scope: {
                condition: '=condition',
            },
            templateUrl: 'components/schedule/schedule-condition.html',
            link: function(scope, el, attrs) {

                var schedules = scope.condition.schedule.schedules;
                scope.time = moment().hours(schedules[0].h[0])
                    .minutes(schedules[0].m[0])
                    .seconds(0)
                    .milliseconds(0)
                    .toDate();

                scope.updateTime = function() {
                    var mtime = moment(scope.time)
                    var hour = mtime.hours()
                    var minute = mtime.minutes()
                    scope.condition.schedule = {
                        schedules: [{
                            h: [hour],
                            m: [minute]
                        }]
                    }
                }

            }
        };
    });
