'use strict';


angular.module('switchApp').factory('WeeklySchedule', function() {

    /**
    * WeeklySchedule class
    * @param {int[]} dows This is an array of scheduled days of the week. Days of the week
    *    are represented as ISO days of the week integers ranging from 1 to 7, where 1 = Monday.
    */
    function WeeklySchedule(dows) {

        this.dows = dows
        this.recipes = []
    }


     WeeklySchedule.prototype.sort = function() {
        this.recipes = _.sortBy(this.recipes, function(recipe){
            var schedules = recipe.conditions[0].schedule.schedules;

            var time = moment().hours(schedules.h[0])
            time.minutes(schedules.m[0])
            return time.toDate().getTime()
        })
    }

    return WeeklySchedule
})
