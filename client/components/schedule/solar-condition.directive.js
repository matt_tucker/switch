'use strict';

/**
 * Weekly Schedule Solar Condition
 */
angular.module('switchApp')
    .directive('solarCondition', function($modal, ScheduleCondition) {
        return {
            restrict: 'E',
            scope: {
                condition: '=condition',
            },
            templateUrl: 'components/schedule/solar-condition.html',
            link: function(scope, el, attrs) {

                scope.solarEvents = ['sunrise',
                    'sunriseEnd',
                    'goldenHourEnd',
                    'solarNoon',
                    'goldenHour',
                    'sunsetStart',
                    'sunset',
                    'dusk',
                    'nauticalDusk',
                    'night',
                    'nadir',
                    'nightEnd',
                    'nauticalDawn',
                    'dawn'
                ]
                scope.times = SunCalc.getTimes(new Date(), 40.0274, -105.2519);

                scope.times = _.mapValues(scope.times, function(time) { return moment(time).format('h:mm:ss a') });

                // console.log('mkt times============', scope.times)


            }
        };
    });
