'use strict';

angular.module('switchApp').factory('DeviceSwitchAction', function(Action, Device) {

    function DeviceSwitchAction(deviceId, value) {
        Action.call(this);

        this.deviceId = deviceId;
        this.value = value || true;
        this.type = 'action.device.zwave.switch'
    };

    DeviceSwitchAction.prototype = Object.create(Action.prototype);

    DeviceSwitchAction.prototype.constructor = DeviceSwitchAction;

    //Implement Abstract Methods
    DeviceSwitchAction.prototype.execute = function(devices) {
        var device = devices[this.deviceId]
        device.setSwitch(this.value);
    }

    return DeviceSwitchAction;
})
