'use strict';

angular.module('switchApp').factory('Action', function() {

    var Action = function() {};

    // Abstract
    Action.prototype.execute = function(devices) {
        alert('execute in Action not implemented');
    };

    return Action
})
