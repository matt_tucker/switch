'use strict';

angular.module('switchApp').factory('JavascriptAction', function(Action, Device) {

    function JavascriptAction() {
        Action.call(this);
        this.script = "console.log('hello world')";
        this.type = 'action.javascript'
    };

    JavascriptAction.prototype = Object.create(Action.prototype);

    JavascriptAction.prototype.constructor = JavascriptAction;

    //Implement Abstract Methods
    JavascriptAction.prototype.execute = function(devices) {
    }

    return JavascriptAction;
})
