'use strict';

angular.module('switchApp').factory('EventCondition', function(Condition) {

    function EventCondition(srcId, srcType, event) {
        Condition.call(this);
        this.type = 'condition.event';
        this.srcId = srcId;
        this.srcType = srcType;
        this.event = event;
    };

    EventCondition.prototype = Object.create(Condition.prototype);

    EventCondition.prototype.constructor = EventCondition;

    //Implement Abstract Methods

    return EventCondition;
})
