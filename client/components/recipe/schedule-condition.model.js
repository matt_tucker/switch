'use strict';

angular.module('switchApp').factory('ScheduleCondition', function(Condition) {

    function ScheduleCondition(obj) {
        Condition.call(this);

        this.schedule = obj.schedule;
        this.type = 'condition.schedule'
    };

    ScheduleCondition.prototype = Object.create(Condition.prototype);

    ScheduleCondition.prototype.constructor = ScheduleCondition;

    //Implement Abstract Methods
    ScheduleCondition.prototype.isValid = function(devices) {
        return true;
    }

    ScheduleCondition.prototype.getSchedule = function() {
        return this.schedule;
    }

    return ScheduleCondition;
})
