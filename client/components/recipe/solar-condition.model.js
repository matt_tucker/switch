'use strict';

angular.module('switchApp').factory('SolarCondition', function(Condition) {

    function SolarCondition(obj) {
        console.log('Creating solar condition', obj)
        Condition.call(this);
        if (!obj) {
            this.event = 'dawn'
        } else {
            this.event = obj.event || 'dawn';
        }

        this.dows = obj.dows
        this.type = 'condition.solar'
    };

    SolarCondition.prototype = Object.create(Condition.prototype);

    SolarCondition.prototype.constructor = SolarCondition;

    //Implement Abstract Methods
    SolarCondition.prototype.isValid = function(devices) {
        return true;
    }

    SolarCondition.prototype.getSchedule = function() {
        var times = SunCalc.getTimes(new Date(), 40.0274, -105.2519);
        var time = times[this.event]

        var mtime = moment(time)
        var hour = mtime.hours()
        var minute = mtime.minutes()
        var schedule = {
            schedules: [{
                h: [hour],
                m: [minute],
                dw: this.dows
            }]
        }

        console.log('Solar schedule:', schedule)
        return schedule;
    }


    return SolarCondition;
})
