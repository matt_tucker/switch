'use strict';

angular.module('switchApp')
    .factory('Recipe', function($resource) {



        function transformRequest(data, headers) {
            if (!data) {
                return data;
            }
            headers = {
                'Content-Type': 'application/json'
            };
            // Gets rid of angular artifacts in the object
            data = angular.copy(data)
            var json = data.toJSON();

            return JSON.stringify(json)
        }

        function transformResponse(data, headers) {
            var obj = JSON.parse(data)

            return obj;
        }

        var Recipe = $resource('/api/recipes/:id/:command', {
            id: '@_id'
        }, {
            update: {
                method: 'PUT',
                transformRequest: transformRequest,
                transformResponse: transformResponse
            },

            run: {
                method: 'POST',
                params: {
                    id: '@_id',
                    command: 'run'
                }
            },
            get: {
                method: 'GET',
                params: {
                    id: ''
                },
                transformRequest: transformRequest,
                transformResponse: transformResponse
            }
        });


        Recipe.prototype.run = function() {
            console.log('Running recipe...')

            this.$run({}, function() {
                console.log('Recipe run successfully.')
            });

        };

        Recipe.prototype.conditions = []
        Recipe.prototype.actions = []

        Recipe.prototype.getBaseType = function() {
            return 'recipe'
        };

        return Recipe;
    });
