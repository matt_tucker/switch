'use strict';

angular.module('switchApp').factory('DeviceRecipe', function(Condition
) {

    function Recipe() {
        this.conditions = [];
        this.actions = [];
    };

    Recipe.prototype.isValid = function(devices, options) {
        return Condition.isAllValid(devices, this.conditions, options)
    };

    Recipe.prototype.execute = function(devices) {
        _.each(this.actions, function(action) {
            action.execute(devices)
        })
    };

    return Recipe;
});
