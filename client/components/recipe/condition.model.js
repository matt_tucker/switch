'use strict';

angular.module('switchApp').factory('Condition', function() {

    var Condition = function() {};

    // Abstract
    Condition.prototype.isValid = function(devices) {
        alert('isValid in Condition not implemented');
    };


    Condition.prototype.isAllValid = function(devices, conditions, options) {

        //filter out conditions that should be ignored.
        if (options.ignore) {
            conditions = _.filter(conditions, function(condition) {
                return !_.contains(options.ignore, condition.type)
            })
        }

        //test the conditions.
        for (var i = 0; i < conditions.length; i++) {
            if (!condition.isValid(devices)) {
                return false;
            }
        }
        return true
    };

    return Condition
})
