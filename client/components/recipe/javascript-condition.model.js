'use strict';

angular.module('switchApp').factory('JavascriptCondition', function(Condition, Device) {

    function JavascriptCondition() {
        Condition.call(this);
        this.script = "console.log('hello world')";
        this.type = 'condition.javascript'
    };

    JavascriptCondition.prototype = Object.create(Condition.prototype);

    JavascriptCondition.prototype.constructor = JavascriptCondition;

    //Implement Abstract Methods


    return JavascriptCondition;
})
