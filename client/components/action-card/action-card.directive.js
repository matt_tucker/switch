    'use strict';

    /**
     * Removes server error when user updates input
     */
    angular.module('switchApp')
        .directive('actionCard', function($modal, Device, Recipe) {
            return {
                restrict: 'E',
                scope: {
                    action: '=action',
                    index: '=index',
                    recipe: '=recipe'
                },
                templateUrl: 'components/action-card/action-card.html',
                link: function(scope, el, attrs) {


                    console.log('Action type =', scope.action.type)

                    switch (scope.action.type) {
                        case 'action.javascript':
                            scope.templateUrl = '/components/action-card/javascript-action-card.html'
                            break;
                        case 'action.device.zwave.switch':
                            scope.devices = Device.query(function(devices) {
                                var device = _.find(devices, function(device) {
                                    return device._id === scope.action.deviceId
                                })

                                if (!device && devices.length > 0) {
                                    console.warn('Couldnt find actions device with id: ', scope.action.deviceId)
                                    device = devices[0]
                                    scope.action.deviceId = device._id
                                }

                                scope.selectedDevice = {device:device}
                            })
                            scope.templateUrl = '/components/action-card/device-switch-action-card.html'

                            break;
                        default:

                    }


                    scope.remove = function() {
                        var title = 'Remove action ' + scope.index + ' ?'

                        var modal = $modal.open({
                            templateUrl: 'components/modal/confirm_modal.html',
                            controller: function($scope) {
                                $scope.title = title;
                                $scope.okLabel = "Remove";
                                $scope.cancelLabel = "Cancel";

                                $scope.ok = function() {
                                    scope.recipe.actions.splice(scope.index, 1);

                                    modal.dismiss('cancel');
                                };

                                $scope.cancel = function() {
                                    modal.dismiss('cancel');
                                };
                            }
                        });
                    }

                    scope.update = function(device) {
                        scope.action.deviceId = device._id
                    }

                }
            };
        });
