'use strict';

/**
 */
angular.module('switchApp')
    .directive('recipeCard', function(Recipe) {
        return {
            restrict: 'E',
            scope: {
                recipe: '=obj'
            },
            templateUrl: 'components/recipe-card/recipe-card.html',
            link: function(scope, el, attrs) {
            }
        };
    });
