'use strict';

angular.module('switchApp')
  .factory('Location', function($resource) {
    return $resource('/api/locations/:id', {
      id: '@_id'
    }, {
      get: {
        method: 'GET',
        params: {
          id: ''
        }
      }
    });
  });
