'use strict';

/**
 * Removes server error when user updates input
 */
angular.module('switchApp')
    .directive('card', function(Device, Recipe, ScheduleCondition, SolarCondition) {
        return {
            restrict: 'E',
            scope: {
                control: '=control'
            },
            templateUrl: 'components/card/card.html',
            link: function(scope, el, attrs) {
                scope.Device = Device
                scope.Recipe = Recipe

                later.date.localTime();

                if (_.startsWith(scope.control.type, 'device')) {
                    scope.device = scope.control
                    scope.controlTemplateUrl = '/components/device/device-card.html'

                    var event = scope.device.getNextEvent()
                    if (event.datetime) {
                        var valueStr = event.value ? 'on' : 'off'
                        scope.message = 'Turning ' + valueStr + ' ' + moment(event.datetime).fromNow() + ' ...'
                    } else {
                        scope.message = ''
                    }


                } else {
                    scope.recipe = scope.control
                    scope.controlTemplateUrl = '/components/recipe/recipe-card.html'
                }

            }
        };
    });
