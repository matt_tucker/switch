angular.module('switchApp').directive('dbDocument', function($window, $http, $rootScope) {

    return {
        restrict: 'E',
        scope: {
            doc: '=',
        },
        templateUrl: '/components/db-document/db-document.html',
        replace: true,
        link: function(scope, el, attrs) {

        }

    }
});
