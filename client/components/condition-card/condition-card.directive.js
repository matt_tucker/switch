    'use strict';

    /**
     * Removes server error when user updates input
     */
    angular.module('switchApp')
        .directive('conditionCard', function($modal, $q, Device, Recipe) {
            return {
                restrict: 'E',
                scope: {
                    condition: '=condition',
                    index: '=index',
                    recipe: '=recipe'
                },
                templateUrl: 'components/condition-card/condition-card.html',
                link: function(scope, el, attrs) {


                    console.log('Condition type =', scope.condition.type)
                    console.log('Condition:', scope.condition)

                    switch (scope.condition.type) {
                        case 'condition.javascript':
                            scope.templateUrl = '/components/condition-card/javascript-condition-card.html'
                            break;

                        case 'condition.event':
                            var devices = Device.query()

                             $q.all([devices.$promise]).then(function(results) {
                                var sources = devices;
                                // sources = sources.concat.apply(sources, results)
                                console.log('sources', sources, results[0])
                                var source = _.find(sources, function(source) {
                                    return source._id === scope.condition.sourceId
                                })

                                scope.selected = {
                                    source: source
                                }

                                scope.sources = sources
                            })

                            scope.eventOptions = []

                            switch (scope.condition.sourceType) {
                                case 'switch':

                                    scope.eventOptions = ['on', 'off']
                                    break;

                                case 'user':
                                    scope.eventOptions = ['home', 'away']
                                    break;
                            }



                            scope.templateUrl = '/components/condition-card/event-condition-card.html'

                            break;
                        default:
                            console.warn('Condition not recognized.')

                    }


                    scope.remove = function() {
                        var title = 'Remove condition ' + scope.index + ' ?'

                        var modal = $modal.open({
                            templateUrl: 'components/modal/confirm_modal.html',
                            controller: function($scope) {
                                $scope.title = title;
                                $scope.okLabel = "Remove";
                                $scope.cancelLabel = "Cancel";

                                $scope.ok = function() {
                                    scope.recipe.conditions.splice(scope.index, 1);

                                    modal.dismiss('cancel');
                                };

                                $scope.cancel = function() {
                                    modal.dismiss('cancel');
                                };
                            }
                        });
                    }

                    scope.update = function(source) {
                        scope.condition.sourceId = source._id
                    }

                }
            };
        });
