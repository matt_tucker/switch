'use strict';

angular.module('switchApp')
    .factory('Device', function($resource, ScheduleCondition, SolarCondition) {


        // var Base = function() {
        //     this.switch = false;
        // }

        function transformRequest(data, headers) {
            if (!data) {
                return data;
            }
            headers = {
                'Content-Type': 'application/json'
            };
            // Gets rid of angular artifacts in the object
            data = angular.copy(data)
            var json = data.toJSON();
            delete json.switch

            return JSON.stringify(json)
        }

        function transformResponse(data, headers) {
            var items = JSON.parse(data)
            var isArray = true
            if (!_.isArray(items)) {
                items = [items]
                isArray = false
            }

            _.each(items, function(obj) {
                if (obj && obj.node && obj.node['classes'] && obj.node['classes'][0x25] && obj.node['classes'][0x25][0]) {
                    obj.switch = obj.node['classes'][0x25][0].value;
                } else {
                    obj.switch = false;
                }
                console.log('switch', obj.switch)
            })

            if (isArray) {
                return items
            } else {
                return items[0]
            }
        }

        var Device = $resource('/api/devices/:id', {
            id: '@_id'
        }, {
            update: {
                method: 'PUT',
                transformRequest: transformRequest,
                transformResponse: transformResponse
            },

            setSwitch: {
                method: 'PUT',
                transformResponse: transformResponse
            },
            get: {
                method: 'GET',
                params: {
                    id: ''
                },
                transformRequest: transformRequest,
                transformResponse: transformResponse
            },
            query: {
                method: 'GET',
                isArray: true,
                transformResponse: transformResponse
            }
        });


        Device.prototype.isSwitch = function() {
            return this.node && this.node['classes'] && this.node['classes'][0x25];
        };


        Device.prototype.setSwitch = function(value) {
            console.log('Setting switch to:', value)

            // if (this.isSwitch() || true) {
            //     this.setNodeSwitchValue(value)
            this.$setSwitch({
                'switch': value
            }, function() {
                console.log('device set switch successfully.', value)
            });

            // }
        };



        Device.prototype.getBaseType = function() {
            return 'device'
        };


        Device.prototype.getNextEvent = function() {
            var nextEvent = null
            var nextValue = null
            try {
                _.each(this.weekly_schedules, function(schedule) {


                    _.each(schedule.recipes, function(recipe) {

                        var condition = recipe.conditions[0]

                        if (condition.type === 'condition.schedule') {
                            condition = new ScheduleCondition(condition)
                        } else if (condition.type === 'condition.solar') {
                            condition = new SolarCondition(condition)
                        } else {
                            console.warn('Unrecognized condition: ', condition.type)
                        }

                        if (!condition) {
                            return
                        }
                        var schedule = condition.getSchedule();

                        var nextOccurrence = later.schedule(schedule).next(1)

                        if (!nextEvent || nextEvent > nextOccurrence) {
                            nextEvent = nextOccurrence
                            nextValue = recipe.actions[0].value
                        }

                    });
                })
            } catch (err) {
                console.log('Failed to find next scheduled event: ', err)
            }

            return {
                datetime: nextEvent,
                value: nextValue
            }
        }


        return Device;
    });
